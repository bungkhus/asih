package com.bungkhus.ministudio.asih.model;

import java.util.Date;

/**
 * Created by ahlul on 07/08/2016.
 */
public class Presensi {
    private int id_presensi;
    private Date tgl_absen;
    private Date pulang;
    private String status_presensi;
    private String alasan;
    private String id_pegawai;
    private int id_jadwal;

    public Presensi() {
    }

    public Date getPulang() {
        return pulang;
    }

    public void setPulang(Date pulang) {
        this.pulang = pulang;
    }

    public int getId_presensi() {
        return id_presensi;
    }

    public void setId_presensi(int id_presensi) {
        this.id_presensi = id_presensi;
    }

    public Date getTgl_absen() {
        return tgl_absen;
    }

    public void setTgl_absen(Date tgl_absen) {
        this.tgl_absen = tgl_absen;
    }

    public String getStatus_presensi() {
        return status_presensi;
    }

    public void setStatus_presensi(String status_presensi) {
        this.status_presensi = status_presensi;
    }

    public String getAlasan() {
        return alasan;
    }

    public void setAlasan(String alasan) {
        this.alasan = alasan;
    }

    public String getId_pegawai() {
        return id_pegawai;
    }

    public void setId_pegawai(String id_pegawai) {
        this.id_pegawai = id_pegawai;
    }

    public int getId_jadwal() {
        return id_jadwal;
    }

    public void setId_jadwal(int id_jadwal) {
        this.id_jadwal = id_jadwal;
    }

    public Presensi(int id_presensi, Date tgl_absen, String status_presensi, String alasan, String id_pegawai, int id_jadwal) {

        this.id_presensi = id_presensi;
        this.tgl_absen = tgl_absen;
        this.status_presensi = status_presensi;
        this.alasan = alasan;
        this.id_pegawai = id_pegawai;
        this.id_jadwal = id_jadwal;
    }
}
