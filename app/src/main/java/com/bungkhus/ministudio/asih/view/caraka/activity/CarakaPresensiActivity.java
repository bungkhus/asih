package com.bungkhus.ministudio.asih.view.caraka.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.adapter.caraka.CarakaPresensiAdapter;
import com.bungkhus.ministudio.asih.helper.DateHelper;
import com.bungkhus.ministudio.asih.helper.SessionManager;
import com.bungkhus.ministudio.asih.model.Presensi;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class CarakaPresensiActivity extends AppCompatActivity {

    private CarakaPresensiAdapter adapter;
    private SessionManager session;
    private CatLoadingView loading;
    private RecyclerView recyclerView;
    private HashMap<String, String> user;
    private List<Presensi> data = new ArrayList<>();
    private String username, status, nama, title;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caraka_presensi);
        session = new SessionManager(getApplicationContext());
        loading = new CatLoadingView();
        user = session.getData();
        username = user.get(SessionManager.KEY_USERNAME);
        status = user.get(SessionManager.KEY_STATUS);
        nama = user.get(SessionManager.KEY_NAMA);
        title = getIntent().getStringExtra("title");
        setupActionBar();
        url = getResources().getString(R.string.ipaddr)+"act=getAllPresensiByUsername&username="+username;
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(CarakaPresensiActivity.this));
        setRecyclerView();
    }
    public void setRecyclerView(){
        data = getAllItemList(url);
        adapter = new CarakaPresensiAdapter(CarakaPresensiActivity.this, data, new CarakaPresensiAdapter.OnItemClickListener() {
            @Override
            public void onClick(Presensi item) {

            }
        }, "caraka");
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }

    private List<Presensi> getAllItemList(final String url){
        final List<Presensi> allItems = new ArrayList<>();
        loading.show(getSupportFragmentManager(),"");

        RequestQueue queue = Volley.newRequestQueue(CarakaPresensiActivity.this);
        StringRequest strReq = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String >(){
                    @Override
                    public void onResponse(String response) {
                        loading.dismiss();
                        Log.d("LOG","Response: "+response.toString());
                        try {
                            JSONArray array_sucess = new JSONArray(response);
                            int size = array_sucess.length();
                            allItems.clear();
                            Presensi model;
                            for(int i = 0; i<size; i++){
                                JSONObject data = array_sucess.getJSONObject(i);
                                int id_presensi = Integer.parseInt(data.getString("id_presensi"));
                                Date tgl_absen = DateHelper.stringToDate(data.getString("tgl_absen"), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
                                String status_presensi = data.getString("status_presensi");
                                String alasan = data.getString("alasan");
                                String id_pegawai = data.getString("id_pegawai");
                                int id_jadwal = Integer.parseInt(data.getString("id_jadwal"));
                                model = new Presensi(id_presensi,tgl_absen,status_presensi,alasan,id_pegawai,id_jadwal);
                                System.out.println("ngeset tgl "+tgl_absen);
                                model.setTgl_absen(tgl_absen);
                                allItems.add(model);
                                adapter.notifyDataSetChanged();
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                Log.e("ERROR", "Error: " + error.getMessage());
                Toast.makeText(CarakaPresensiActivity.this,
                        "Gagal Menampilkan Data.", Toast.LENGTH_LONG).show();
            }
        });

        // Adding request to request queue
        queue.add(strReq);

        return allItems;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_no_animation, R.anim.push_out_right);
    }

    void setupActionBar(){
        getSupportActionBar().setElevation(3);
        getSupportActionBar().setTitle(title.toUpperCase());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

    }
}
