package com.bungkhus.ministudio.asih.adapter.caraka;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.helper.DateHelper;
import com.bungkhus.ministudio.asih.model.Presensi;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ahlul on 07/08/2016.
 */
public class CarakaPresensiAdapter extends RecyclerView.Adapter<CarakaPresensiAdapter.RecyclerViewHolders>{

    private final OnItemClickListener listener;
    private List<Presensi> itemList;
    private Context context;
    private String sebagai;
    private String pulang = "";

    public CarakaPresensiAdapter(Context context, List<Presensi> itemList, OnItemClickListener listener, String sebagai) {
        this.itemList = itemList;
        this.context = context;
        this.listener = listener;
        this.sebagai = sebagai;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_caraka_presensi, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolders holder, final int position) {
        holder.click(itemList.get(position), listener);
        holder.tvTanggal.setText(DateHelper.dateToString(itemList.get(position).getTgl_absen(), new SimpleDateFormat("EEEE, dd MMM yyyy, HH:mm")));
        System.out.println("ikilo :"+itemList.get(position).getTgl_absen());
        holder.tvStatus.setText(itemList.get(position).getStatus_presensi().toUpperCase());
        holder.tvTanggal.setBackgroundColor(context.getResources().getColor(R.color.white));

        if(sebagai.equalsIgnoreCase("satpam") || itemList.get(position).getStatus_presensi().equalsIgnoreCase("Ijin") || itemList.get(position).getStatus_presensi().equalsIgnoreCase("Alpha")){
            holder.tvAlasan.setVisibility(View.GONE);
        }else{
            holder.tvAlasan.setVisibility(View.VISIBLE);
            holder.tvTanggal.setTextColor(context.getResources().getColor(R.color.primary_text));
        }

        if(itemList.get(position).getAlasan().length() > 1){
            holder.tvStatus.append(" ("+itemList.get(position).getAlasan()+")");
        }
        if(itemList.get(position).getStatus_presensi().equalsIgnoreCase("hadir")){
            holder.tvStatus.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.btPulang.setVisibility(View.VISIBLE);
            Date masuk = itemList.get(position).getTgl_absen();
            Date pulang = itemList.get(position).getPulang();
            if(pulang.after(masuk)){
                holder.tvTanggal.append(" s.d.\n"+DateHelper.dateToString(pulang, new SimpleDateFormat("dd MMM yyyy, HH:mm")));
                holder.btPulang.setEnabled(false);
                holder.btPulang.setText("JAM KERJA: "+hitungJamKerja(masuk, pulang));
                holder.btPulang.setBackgroundColor(context.getResources().getColor(R.color.samar));
                holder.btPulang.setTextColor(context.getResources().getColor(R.color.primary_text));
            }else{
                holder.btPulang.setEnabled(true);
                holder.btPulang.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));
                holder.btPulang.setTextColor(context.getResources().getColor(R.color.white));
                holder.btPulang.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setJamKeluarDialog(String.valueOf(itemList.get(position).getId_presensi()), holder.btPulang);
                    }
                });
            }
        }else{
            holder.btPulang.setVisibility(View.GONE);
            holder.tvStatus.setTextColor(context.getResources().getColor(R.color.orange));
        }
    }

    public void setJamKeluarDialog(final String id_presensi, final Button btPulang) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        final View dialogView = inflater.inflate(R.layout.custom_dialog_jam_keluar, null);
        dialogBuilder.setView(dialogView);

        final EditText etJamKeluar = (EditText) dialogView.findViewById(R.id.etJamKeluar);

        final SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        final SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final Calendar c = Calendar.getInstance();
        String jam = dateFormat2.format(c.getTime());
        etJamKeluar.setText(jam);
        etJamKeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog d = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar c2 = Calendar.getInstance();
                        c2.set(year, monthOfYear, dayOfMonth);
                        pulang = dateFormat1.format(c2.getTime());
                        TimePickerDialog t = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                String jam = String.valueOf(hourOfDay);
                                String menit = String.valueOf(minute);
                                if(jam.length() == 1){
                                    jam = "0"+hourOfDay;
                                }
                                if(menit.length() == 1){
                                    menit = "0"+minute;
                                }
                                pulang = pulang+" "+jam + ":" + menit + ":00";
                                etJamKeluar.setText(pulang);
                            }
                        }, c.get(Calendar.HOUR), c.get(Calendar.MINUTE), true);
                        t.show();
                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                d.show();
            }
        });

        dialogBuilder.setTitle("PULANG");
        dialogBuilder.setPositiveButton("SIMPAN", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                postData(id_presensi, etJamKeluar.getText().toString(), btPulang);
            }
        });
        dialogBuilder.setNegativeButton("Kembali", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    private void postData(final String id_presensi, final String etJamKeluar, final Button btPulang){
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest strReq = new StringRequest(Request.Method.POST,
                context.getResources().getString(R.string.ipaddr)+"act=updatePulang", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("success");

                    // Check for error node in json
                    if (status.equalsIgnoreCase("1")) {
                        Toast.makeText(context,
                                "Waktu pulang berhasil terkirim!", Toast.LENGTH_LONG).show();
                        btPulang.setEnabled(false);
                        btPulang.setText("Silahkan Perbaharui Data...");
                        btPulang.setBackgroundColor(context.getResources().getColor(R.color.samar));
                        btPulang.setTextColor(context.getResources().getColor(R.color.primary_text));
                    } else {
                        // Error in login. Get the error message
                        Toast.makeText(context,
                                "Gagal mengirim data!", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Toast.makeText(context,
                            "Gagal mengirim data!", Toast.LENGTH_LONG).show();
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", "Submit Error: " + error.getMessage());
                Toast.makeText(context,
                        "Submit Failed", Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("pulang", etJamKeluar);
                params.put("id_presensi", id_presensi);
                return params;
            }

        };

        // Adding request to request queue
        queue.add(strReq);
    }

    private String hitungJamKerja(Date d1, Date d2){

        String jamKerja = "-";

        try {
            //in milliseconds
            long diff = d2.getTime() - d1.getTime();
            System.out.println("tess"+diff);

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);
            jamKerja = diffHours+" jam, "+diffMinutes+" menit.";
        } catch (Exception e) {
            e.printStackTrace();
        }

        return jamKerja;

    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView  tvTanggal, tvStatus, tvAlasan;
        private Button btPulang;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvTanggal = (TextView)itemView.findViewById(R.id.tvTanggal);
            tvStatus = (TextView)itemView.findViewById(R.id.tvStatus);
            tvAlasan = (TextView)itemView.findViewById(R.id.tvAlasan);
            btPulang = (Button)itemView.findViewById(R.id.btPulang);
        }

        public void click(final Presensi item, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(item);
                }
            });
        }

        @Override
        public void onClick(View view) {

        }
    }

    public interface OnItemClickListener {
        void onClick(Presensi item);
    }
}