package com.bungkhus.ministudio.asih.model;

/**
 * Created by ahlul on 07/08/2016.
 */
public class Penilaian {
    private int id_penilaian;
    private int id_presensi;
    private String periode;
    private  double absensi;
    private  String id_pegawai;
    private int kesigapan;
    private int patroli;
    private int layanan_tamu;
    private  int keramahan;
    private int kerajinan;
    private int kebersihan;
    private Double nilai_akhir;

    public Penilaian() {
    }

    public Penilaian(int id_penilaian, int id_presensi, String periode, double absensi, String id_pegawai, int kesigapan, int patroli, int layanan_tamu, int keramahan, int kerajinan, int kebersihan, Double nilai_akhir) {
        this.id_penilaian = id_penilaian;
        this.id_presensi = id_presensi;
        this.periode = periode;
        this.absensi = absensi;
        this.id_pegawai = id_pegawai;
        this.kesigapan = kesigapan;
        this.patroli = patroli;
        this.layanan_tamu = layanan_tamu;
        this.keramahan = keramahan;
        this.kerajinan = kerajinan;
        this.kebersihan = kebersihan;
        this.nilai_akhir = nilai_akhir;
    }

    public int getId_penilaian() {
        return id_penilaian;
    }

    public void setId_penilaian(int id_penilaian) {
        this.id_penilaian = id_penilaian;
    }

    public int getId_presensi() {
        return id_presensi;
    }

    public void setId_presensi(int id_presensi) {
        this.id_presensi = id_presensi;
    }

    public String getPeriode() {
        return periode;
    }

    public void setPeriode(String periode) {
        this.periode = periode;
    }

    public double getAbsensi() {
        return absensi;
    }

    public void setAbsensi(double absensi) {
        this.absensi = absensi;
    }

    public String getId_pegawai() {
        return id_pegawai;
    }

    public void setId_pegawai(String id_pegawai) {
        this.id_pegawai = id_pegawai;
    }

    public int getKesigapan() {
        return kesigapan;
    }

    public void setKesigapan(int kesigapan) {
        this.kesigapan = kesigapan;
    }

    public int getPatroli() {
        return patroli;
    }

    public void setPatroli(int patroli) {
        this.patroli = patroli;
    }

    public int getLayanan_tamu() {
        return layanan_tamu;
    }

    public void setLayanan_tamu(int layanan_tamu) {
        this.layanan_tamu = layanan_tamu;
    }

    public int getKeramahan() {
        return keramahan;
    }

    public void setKeramahan(int keramahan) {
        this.keramahan = keramahan;
    }


    public int getKerajinan() {
        return kerajinan;
    }

    public void setKerajinan(int kerajinan) {
        this.kerajinan = kerajinan;
    }

    public int getKebersihan() {
        return kebersihan;
    }

    public void setKebersihan(int kebersihan) {
        this.kebersihan = kebersihan;
    }

    public Double getNilai_akhir() {
        return nilai_akhir;
    }

    public void setNilai_akhir(Double nilai_akhir) {
        this.nilai_akhir = nilai_akhir;
    }
}
