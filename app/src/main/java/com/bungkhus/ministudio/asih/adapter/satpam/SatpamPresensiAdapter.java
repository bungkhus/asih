package com.bungkhus.ministudio.asih.adapter.satpam;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.helper.DateHelper;
import com.bungkhus.ministudio.asih.model.Presensi;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by ahlul on 07/08/2016.
 */
public class SatpamPresensiAdapter extends RecyclerView.Adapter<SatpamPresensiAdapter.RecyclerViewHolders>{

    private final OnItemClickListener listener;
    private List<Presensi> itemList;
    private Context context;

    public SatpamPresensiAdapter(Context context, List<Presensi> itemList, OnItemClickListener listener) {
        this.itemList = itemList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_satpam_presensi, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        holder.click(itemList.get(position), listener);
        holder.tvTanggal.setText(DateHelper.dateToString(itemList.get(position).getTgl_absen(), new SimpleDateFormat("dd-MMM-yyy HH:mm")));
        System.out.println("ikilo :"+itemList.get(position).getTgl_absen());
        holder.tvStatus.setText("Status: "+itemList.get(position).getStatus_presensi());
        holder.tvAlasan.setText("Alasan: "+itemList.get(position).getAlasan());
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView tvTanggal, tvStatus, tvAlasan;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvTanggal = (TextView)itemView.findViewById(R.id.tvTanggal);
            tvStatus = (TextView)itemView.findViewById(R.id.tvStatus);
            tvAlasan = (TextView)itemView.findViewById(R.id.tvAlasan);
        }

        public void click(final Presensi item, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(item);
                }
            });
        }

        @Override
        public void onClick(View view) {

        }
    }

    public interface OnItemClickListener {
        void onClick(Presensi item);
    }
}
