package com.bungkhus.ministudio.asih.adapter.satpam;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.helper.DateHelper;
import com.bungkhus.ministudio.asih.model.Jadwal;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by ahlul on 07/08/2016.
 */
public class SatpamJadwalAdapter extends RecyclerView.Adapter<SatpamJadwalAdapter.RecyclerViewHolders> {

    private final OnItemClickListener listener;
    private List<Jadwal> itemList;
    private Context context;

    public SatpamJadwalAdapter(Context context, List<Jadwal> itemList, OnItemClickListener listener) {
        this.itemList = itemList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_satpam_jadwal, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        holder.click(itemList.get(position), listener);
        String tanggal = DateHelper.dateToString(itemList.get(position).getTgl_mulai(), new SimpleDateFormat("dd MMM yyyy"))+" s.d. "+DateHelper.dateToString(itemList.get(position).getTgl_selesai(), new SimpleDateFormat("dd MMM yyyy"));
        holder.tvTanggal.setText(tanggal);
        holder.tvTugas.setText(itemList.get(position).getTugas());
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvTugas, tvTanggal;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvTugas = (TextView) itemView.findViewById(R.id.tvTugas);
            tvTanggal = (TextView) itemView.findViewById(R.id.tvTanggal);
        }

        public void click(final Jadwal item, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(item);
                }
            });
        }

        @Override
        public void onClick(View view) {

        }
    }

    public interface OnItemClickListener {
        void onClick(Jadwal item);
    }
}
