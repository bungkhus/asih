package com.bungkhus.ministudio.asih.view.tatausaha.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bungkhus.ministudio.asih.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class KatuKegiatanKebersihanPegawaiFragment extends Fragment {


    public KatuKegiatanKebersihanPegawaiFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_katu_kegiatan_kebersihan_pegawai, container, false);
    }

}
