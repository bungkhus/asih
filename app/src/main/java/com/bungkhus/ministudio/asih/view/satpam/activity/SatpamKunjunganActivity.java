package com.bungkhus.ministudio.asih.view.satpam.activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.adapter.satpam.SatpamKunjunganAdapter;
import com.bungkhus.ministudio.asih.adapter.satpam.SatpamPenilaianAdapter;
import com.bungkhus.ministudio.asih.adapter.tatausaha.KatuKunjunganAdapter;
import com.bungkhus.ministudio.asih.helper.DateHelper;
import com.bungkhus.ministudio.asih.helper.SessionManager;
import com.bungkhus.ministudio.asih.model.Kunjungan;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SatpamKunjunganActivity extends AppCompatActivity {

    private KatuKunjunganAdapter adapter;
    private SessionManager session;
    private CatLoadingView loading;
    private RecyclerView recyclerView;
    private HashMap<String, String> user;
    private List<Kunjungan> data = new ArrayList<>();
    private String username, status, nama, title;
    String url, url_update;
    final Calendar calendar = Calendar.getInstance();
    private TextView tvTanggal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_satpam_kunjungan);
        session = new SessionManager(getApplicationContext());
        loading = new CatLoadingView();
        user = session.getData();
        username = user.get(SessionManager.KEY_USERNAME);
        status = user.get(SessionManager.KEY_STATUS);
        nama = user.get(SessionManager.KEY_NAMA);
        title = getIntent().getStringExtra("title");
        setupActionBar();
        tvTanggal = (TextView) findViewById(R.id.tvTanggal);
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, dd MMM yyyy");
        tvTanggal.setText(dateFormat.format(calendar.getTime()));
        setupActionBar();
        url = getResources().getString(R.string.ipaddr)+"act=getAllKunjunganByDate&tgl=";
        url_update = getResources().getString(R.string.ipaddr)+"act=updateJamKunjungan";
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(SatpamKunjunganActivity.this));
        setRecyclerView(DateHelper.dateToString(DateHelper.stringToDate(tvTanggal.getText().toString(), new SimpleDateFormat("EEEE, dd MMM yyyy")), new SimpleDateFormat("yyyy-MM-dd")));
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRecyclerView(DateHelper.dateToString(DateHelper.stringToDate(tvTanggal.getText().toString(), new SimpleDateFormat("EEEE, dd MMM yyyy")), new SimpleDateFormat("yyyy-MM-dd")));
    }

    public void setRecyclerView(String tgl) {
        String final_url = url+tgl;
        data = getAllItemList(final_url);
        adapter = new KatuKunjunganAdapter(SatpamKunjunganActivity.this, data, new KatuKunjunganAdapter.OnItemClickListener() {
            @Override
            public void onClick(Kunjungan item) {
                String title = item.getNama();
                String msg = "Alamat: "+item.getAlamat()
                        +"\n\nNo. HP: "+item.getNoHp()
                        +"\n\nJam Keluar: "+item.getJam_keluar();
                if(item.getJam_keluar().length() > 1){
                    viewMore(title, msg, String.valueOf(item.getId_kunjungan()));
                }else{
                    viewMoreplus(title, msg, String.valueOf(item.getId_kunjungan()));
                }
            }
        });
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }

    private List<Kunjungan> getAllItemList(final String url){
        final List<Kunjungan> allItems = new ArrayList<>();
//        loading.show(getSupportFragmentManager(),"");

        RequestQueue queue = Volley.newRequestQueue(SatpamKunjunganActivity.this);
        StringRequest strReq = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String >(){
                    @Override
                    public void onResponse(String response) {
//                        loading.dismiss();
                        Log.d("LOG","Response: "+response.toString());
                        try {
                            JSONArray array_sucess = new JSONArray(response);
                            int size = array_sucess.length();
                            allItems.clear();
                            Kunjungan model;
                            for(int i = 0; i<size; i++){
                                JSONObject data = array_sucess.getJSONObject(i);
                                int id_kunjungan = Integer.parseInt(data.getString("id_kunjungan"));
                                Date tgl_kunjungan = DateHelper.stringToDate(data.getString("tgl_kunjungan"),new SimpleDateFormat("yyyy-MM-dd"));
                                String jam_kunjungan = data.getString("jam_kunjungan");
                                String nama = data.getString("nama");
                                String institusi = data.getString("institusi");
                                String agenda = data.getString("agenda");
                                int jumlah_rombongan = Integer.parseInt(data.getString("jumlah_rombongan"));
                                String username = data.getString("username");
                                String no_hp = data.getString("no_hp");
                                String alamat = data.getString("alamat");
                                String jam_keluar;
                                if(data.getString("jam_keluar").equalsIgnoreCase("null") || data.getString("jam_keluar") == null){
                                    jam_keluar = "-";
                                }else{
                                    jam_keluar = data.getString("jam_keluar");
                                }
                                model = new Kunjungan(id_kunjungan,tgl_kunjungan,jam_kunjungan,nama,institusi,agenda,jumlah_rombongan,username);
                                model.setAlamat(alamat);
                                model.setNoHp(no_hp);
                                model.setJumlah_rombongan(jumlah_rombongan);
                                model.setJam_keluar(jam_keluar);
                                allItems.add(model);
                                adapter.notifyDataSetChanged();
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                loading.dismiss();
                Log.e("ERROR", "Error: " + error.getMessage());
                Toast.makeText(SatpamKunjunganActivity.this,
                        "Gagal Menampilkan Data.", Toast.LENGTH_LONG).show();
            }
        });

        // Adding request to request queue
        queue.add(strReq);

        return allItems;
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.kunjungan, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_add:
                goTo(SatpamInputKunjunganActivity.class, "CATAT KUNJUNGAN");
                break;
            case R.id.action_date:
                showDatePicker();
                break;
        }
        return true;
    }

    private void showDatePicker(){
        DatePickerDialog d = new DatePickerDialog(SatpamKunjunganActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar c2 = Calendar.getInstance();
                c2.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, dd MMM yyyy");
                tvTanggal.setText(dateFormat.format(c2.getTime()));
                setRecyclerView(DateHelper.dateToString(DateHelper.stringToDate(tvTanggal.getText().toString(), new SimpleDateFormat("EEEE, dd MMM yyyy")), new SimpleDateFormat("yyyy-MM-dd")));
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        d.show();
    }

    void goTo(Class<?> cls, String title){
        Intent i = new Intent(SatpamKunjunganActivity.this, cls);
        i.putExtra("title", title);
        startActivity(i);
        overridePendingTransition(R.anim.pull_in_right, R.anim.activity_no_animation);
    }

    void viewMoreplus(String title, String msg, final String id_kunjungan){
        new AlertDialog.Builder(SatpamKunjunganActivity.this)
                .setTitle(title)
                .setMessage(msg)
                .setNegativeButton("KEMBALI", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //
                    }
                })
                .setPositiveButton("ISI JAM KELUAR", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        setJamKeluarDialog(id_kunjungan);
                    }
                })
                .show();
    }

    void viewMore(String title, String msg, final String id_kunjungan){
        new AlertDialog.Builder(SatpamKunjunganActivity.this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton("KEMBALI", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //
                    }
                })
                .show();
    }

    public void setJamKeluarDialog(final String id_kunjungan) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog_jam_keluar, null);
        dialogBuilder.setView(dialogView);

        final EditText etJamKeluar = (EditText) dialogView.findViewById(R.id.etJamKeluar);

        SimpleDateFormat dateFormat2 = new SimpleDateFormat("HH:mm");
        final Calendar c = Calendar.getInstance();
        String jam = dateFormat2.format(c.getTime());
        etJamKeluar.setText(jam);
        etJamKeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog t = new TimePickerDialog(SatpamKunjunganActivity.this, new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String jamFinal = hourOfDay + ":" + minute;
                        etJamKeluar.setText(jamFinal);
                    }
                }, c.get(Calendar.HOUR), c.get(Calendar.MINUTE), true);
                t.show();
            }
        });

        dialogBuilder.setTitle("JAM KELUAR");
        dialogBuilder.setPositiveButton("SIMPAN", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                postData(id_kunjungan, etJamKeluar.getText().toString());
            }
        });
        dialogBuilder.setNegativeButton("Kembali", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    private void postData(final String id_kunjungan, final String etJamKeluar){
        RequestQueue queue = Volley.newRequestQueue(SatpamKunjunganActivity.this);
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url_update, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("success");

                    // Check for error node in json
                    if (status.equalsIgnoreCase("1")) {
                        Toast.makeText(SatpamKunjunganActivity.this,
                                "Jam keluar berhasil terkirim!", Toast.LENGTH_LONG).show();
                        setRecyclerView(DateHelper.dateToString(DateHelper.stringToDate(tvTanggal.getText().toString(), new SimpleDateFormat("EEEE, dd MMM yyyy")), new SimpleDateFormat("yyyy-MM-dd")));
                    } else {
                        // Error in login. Get the error message
                        Toast.makeText(SatpamKunjunganActivity.this,
                                "Gagal mengirim data!", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Toast.makeText(SatpamKunjunganActivity.this,
                            "Gagal mengirim data!", Toast.LENGTH_LONG).show();
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", "Submit Error: " + error.getMessage());
                Toast.makeText(SatpamKunjunganActivity.this,
                        "Submit Failed", Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("jam_keluar", etJamKeluar);
                params.put("id_kunjungan", id_kunjungan);
                return params;
            }

        };

        // Adding request to request queue
        queue.add(strReq);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_no_animation, R.anim.push_out_right);
    }

    void setupActionBar(){
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setTitle(title.toUpperCase());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

    }
}
