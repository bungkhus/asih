package com.bungkhus.ministudio.asih.view.tatausaha.activity;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.adapter.caraka.CarakaPengajuanAlatAdapter;
import com.bungkhus.ministudio.asih.helper.DateHelper;
import com.bungkhus.ministudio.asih.helper.SessionManager;
import com.bungkhus.ministudio.asih.model.PengajuanAlat;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KatuAlatActivity extends AppCompatActivity {

    private CarakaPengajuanAlatAdapter adapter;
    private SessionManager session;
    private CatLoadingView loading;
    private RecyclerView recyclerView;
    private HashMap<String, String> user;
    private List<PengajuanAlat> data = new ArrayList<>();
    private String username, status, nama, title;
    String url, url_update;
    private Spinner spAlat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caraka_alat);
        session = new SessionManager(getApplicationContext());
        loading = new CatLoadingView();
        user = session.getData();
        username = user.get(SessionManager.KEY_USERNAME);
        status = user.get(SessionManager.KEY_STATUS);
        nama = user.get(SessionManager.KEY_NAMA);
        title = getIntent().getStringExtra("title");
        setupActionBar();
        url = getResources().getString(R.string.ipaddr)+"act=getAllAlatByProgress&progress=";
        url_update = getResources().getString(R.string.ipaddr)+"act=updateStatusJadwal";
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(KatuAlatActivity.this));
        spAlat = (Spinner) findViewById(R.id.spAlat);
        spAlat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String progress = spAlat.getSelectedItem().toString();
                if(progress.equals("Tidak Tersedia")){
                    progress = "Tidak%20Tersedia";
                }
                setRecyclerView(progress);
                System.out.println("iki - "+url+progress);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    public void setRecyclerView(String status){
        data = getAllItemList(url+status);
        adapter = new CarakaPengajuanAlatAdapter(KatuAlatActivity.this, data, new CarakaPengajuanAlatAdapter.OnItemClickListener() {
            @Override
            public void onClick(PengajuanAlat item) {
                showChangeLangDialog(item.getNama_barang(), item.getMerk_barang(), String.valueOf(item.getJumlah_barang()), item.getIddetail());
            }
        }, "katu");
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }

    public void showChangeLangDialog(String nama_barang, String merk, String jumlal, final String iddetail) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog_edit_status_alat, null);
        dialogBuilder.setView(dialogView);

        final Spinner spAlat = (Spinner) dialogView.findViewById(R.id.spAlat);

        dialogBuilder.setTitle("UPDATE STATUS");
        dialogBuilder.setMessage(jumlal+" "+nama_barang+" "+merk);
        dialogBuilder.setPositiveButton("SIMPAN", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                postData(iddetail, spAlat.getSelectedItem().toString());
            }
        });
        dialogBuilder.setNegativeButton("Kembali", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    private void postData(final String iddetail, final String progress){
        RequestQueue queue = Volley.newRequestQueue(KatuAlatActivity.this);
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url_update, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("success");

                    // Check for error node in json
                    if (status.equalsIgnoreCase("1")) {
                        Toast.makeText(KatuAlatActivity.this,
                                "Update status berhasil terkirim!", Toast.LENGTH_LONG).show();
                    } else {
                        // Error in login. Get the error message
                        Toast.makeText(KatuAlatActivity.this,
                                "Gagal mengirim data!", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Toast.makeText(KatuAlatActivity.this,
                            "Gagal mengirim data!", Toast.LENGTH_LONG).show();
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", "Submit Error: " + error.getMessage());
                Toast.makeText(KatuAlatActivity.this,
                        "Submit Failed", Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("iddetail", iddetail);
                params.put("progress", progress);
                return params;
            }

        };

        // Adding request to request queue
        queue.add(strReq);
    }

    private List<PengajuanAlat> getAllItemList(final String url){
        final List<PengajuanAlat> allItems = new ArrayList<>();
        loading.show(getSupportFragmentManager(),"");

        RequestQueue queue = Volley.newRequestQueue(KatuAlatActivity.this);
        StringRequest strReq = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String >(){
                    @Override
                    public void onResponse(String response) {
                        loading.dismiss();
                        Log.d("LOG","Response: "+response.toString());
                        try {
                            JSONArray array_sucess = new JSONArray(response);
                            int size = array_sucess.length();
                            allItems.clear();
                            PengajuanAlat model;
                            for(int i = 0; i<size; i++){
                                JSONObject data = array_sucess.getJSONObject(i);
                                String username = data.getString("username");
                                Date tgl_pengajuan = DateHelper.stringToDate(data.getString("tgl_pengajuan"), new SimpleDateFormat("yyyy-MM-dd"));
                                String nama_barang = data.getString("nama_barang");
                                String merk_barang= data.getString("merk_barang");
                                int jumlah_barang = Integer.parseInt(data.getString("jumlah_permintaan"));
                                model = new PengajuanAlat(username,tgl_pengajuan,nama_barang,merk_barang,jumlah_barang);
                                model.setTgl_pengajuan(tgl_pengajuan);
                                model.setIddetail(data.getString("iddetail"));
                                allItems.add(model);
                                adapter.notifyDataSetChanged();
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                Log.e("ERROR", "Error: " + error.getMessage());
                Toast.makeText(KatuAlatActivity.this,
                        "Gagal Menampilkan Data.", Toast.LENGTH_LONG).show();
            }
        });

        // Adding request to request queue
        queue.add(strReq);

        return allItems;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_no_animation, R.anim.push_out_right);
    }

    void setupActionBar(){
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setTitle(title.toUpperCase());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

    }
}
