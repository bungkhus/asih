package com.bungkhus.ministudio.asih.adapter.tatausaha;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.helper.DateHelper;
import com.bungkhus.ministudio.asih.model.Kunjungan;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by bungkhus on 8/7/16.
 */
public class KatuKunjunganAdapter extends RecyclerView.Adapter<KatuKunjunganAdapter.RecyclerViewHolders> {

    private final OnItemClickListener listener;
    private List<Kunjungan> itemList;
    private Context context;

    public KatuKunjunganAdapter(Context context, List<Kunjungan> itemList, OnItemClickListener listener) {
        this.itemList = itemList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_katu_kunjungan, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        holder.click(itemList.get(position), listener);
        holder.tvInstitusi.setText(itemList.get(position).getNama()+" ("+itemList.get(position).getInstitusi()+")");
        holder.tvTanggal.setText(itemList.get(position).getAgenda());
        holder.tvAgenda.setText(DateHelper.dateToString(itemList.get(position).getTgl_kunjungan(), new SimpleDateFormat("dd MMM yyyy"))+", "+itemList.get(position).getJam_kunjungan());
        holder.tvJam.setVisibility(View.GONE);
        holder.tvNama.setVisibility(View.GONE);
        holder.tvRombongan.setText(itemList.get(position).getJumlah_rombongan()+" Orang");
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView tvInstitusi, tvTanggal, tvJam, tvNama, tvRombongan, tvAgenda;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvInstitusi = (TextView)itemView.findViewById(R.id.tvInstitusi);
            tvTanggal = (TextView)itemView.findViewById(R.id.tvTanggal);
            tvJam = (TextView)itemView.findViewById(R.id.tvJam);
            tvNama = (TextView)itemView.findViewById(R.id.tvNama);
            tvRombongan = (TextView)itemView.findViewById(R.id.tvRombongan);
            tvAgenda = (TextView)itemView.findViewById(R.id.tvAgenda);
        }

        public void click(final Kunjungan item, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(item);
                }
            });
        }

        @Override
        public void onClick(View view) {

        }
    }

    public interface OnItemClickListener {
        void onClick(Kunjungan item);
    }
}