package com.bungkhus.ministudio.asih.view.satpam.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.adapter.tatausaha.KatuPeristiwaAdapter;
import com.bungkhus.ministudio.asih.helper.DateHelper;
import com.bungkhus.ministudio.asih.helper.SessionManager;
import com.bungkhus.ministudio.asih.model.Peristiwa;
import com.bungkhus.ministudio.asih.view.LoginActivity;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SatpamPeristiwaActivity extends AppCompatActivity {

    private HashMap<String, String> user;
    private String username, status, nama, title;
    private SessionManager session;
    private CatLoadingView loading;
    private RecyclerView recyclerView;
    private List<Peristiwa> data = new ArrayList<>();
    private KatuPeristiwaAdapter adapter;
    String url;
    private Spinner spBln, spThn;
    private String bulan, tahun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_satpam_peristiwa);
        session = new SessionManager(getApplicationContext());
        loading = new CatLoadingView();
        spBln = (Spinner) findViewById(R.id.spBln);
        spThn = (Spinner) findViewById(R.id.spThn);
        user = session.getData();
        username = user.get(SessionManager.KEY_USERNAME);
        status = user.get(SessionManager.KEY_STATUS);
        nama = user.get(SessionManager.KEY_NAMA);
        title = getIntent().getStringExtra("title");
        setupActionBar();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(SatpamPeristiwaActivity.this));
        setupSpinner();
    }

    private void setupSpinner(){
        final Calendar c = Calendar.getInstance();
        bulan = new SimpleDateFormat("MM").format(c.getTime());
        tahun = new SimpleDateFormat("yyyy").format(c.getTime());

        spBln.setSelection((Integer.parseInt(bulan) - 1));
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(SatpamPeristiwaActivity.this, R.array.thn, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spThn.setAdapter(adapter);
        if (!tahun.equals(null)) {
            int spinnerPosition = adapter.getPosition(tahun);
            spThn.setSelection(spinnerPosition);
        }
        spBln.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int bln = adapterView.getSelectedItemPosition()+1;
                url = getResources().getString(R.string.ipaddr)+"act=getAllPeristiwaByPeriode&bln="+bln+"&thn="+spThn.getSelectedItem();
                setRecyclerView(url);
                System.out.println("anu.."+url);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spThn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int bln = spBln.getSelectedItemPosition()+1;
                url = getResources().getString(R.string.ipaddr)+"act=getAllPeristiwaByPeriode&bln="+bln+"&thn="+adapterView.getSelectedItem();
                setRecyclerView(url);
                System.out.println("emm.."+url);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    public void setRecyclerView(String url) {
        data = getAllItemList(url);
        adapter = new KatuPeristiwaAdapter(SatpamPeristiwaActivity.this, data, new KatuPeristiwaAdapter.OnItemClickListener() {
            @Override
            public void onClick(Peristiwa item) {
                if(item.getStatus_penyelesaian().equalsIgnoreCase("belum")){
                    alertUpdate(String.valueOf(item.getId_peristiwa()));
                }
            }
        }, "satpam");
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }

    void alertUpdate(final String id_peristiwa){
        new AlertDialog.Builder(SatpamPeristiwaActivity.this)
                .setTitle("UBAH STATUS PERISTIWA")
                .setMessage("Apakah anda yakin untuk merubah status peristiwa menjadi: SELESAI?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        updateStatus(id_peristiwa);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(R.drawable.alert)
                .show();
    }

    private void updateStatus(final String id_peristiwa){
        loading.show(getSupportFragmentManager(), "");
        RequestQueue queue = Volley.newRequestQueue(SatpamPeristiwaActivity.this);
        StringRequest strReq = new StringRequest(Request.Method.POST,
                getResources().getString(R.string.ipaddr)+"act=updateStatusPeristiwa", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response);
                loading.dismiss();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("success");

                    // Check for error node in json
                    if (status.equalsIgnoreCase("1")) {
                        Toast.makeText(SatpamPeristiwaActivity.this,
                                "Update status berhasil terkirim!", Toast.LENGTH_LONG).show();
                        int bln = spBln.getSelectedItemPosition()+1;
                        url = getResources().getString(R.string.ipaddr)+"act=getAllPeristiwaByPeriode&bln="+bln+"&thn="+spThn.getSelectedItem();
                        setRecyclerView(url);
                    } else {
                        // Error in login. Get the error message
                        Toast.makeText(SatpamPeristiwaActivity.this,
                                "Gagal mengirim data!", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Toast.makeText(SatpamPeristiwaActivity.this,
                            "Gagal mengirim data!", Toast.LENGTH_LONG).show();
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                Log.e("ERROR", "Submit Error: " + error.getMessage());
                Toast.makeText(SatpamPeristiwaActivity.this,
                        "Submit Failed", Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_peristiwa", id_peristiwa);
                return params;
            }

        };

        // Adding request to request queue
        queue.add(strReq);
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.add, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_add:
                goTo(SatpamInputPeristiwaActivity.class, "INPUT PERISTIWA");
                break;
        }
        return true;
    }

    void goTo(Class<?> cls, String title){
        Intent i = new Intent(SatpamPeristiwaActivity.this, cls);
        i.putExtra("title", title);
        startActivity(i);
        overridePendingTransition(R.anim.pull_in_right, R.anim.activity_no_animation);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_no_animation, R.anim.push_out_right);
    }

    void setupActionBar(){
        getSupportActionBar().setElevation(3);
        getSupportActionBar().setTitle(title.toUpperCase());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

    }

    private List<Peristiwa> getAllItemList(final String url){

        final List<Peristiwa> allItems = new ArrayList<>();
//        loading.show(getSupportFragmentManager(), "");

        RequestQueue queue = Volley.newRequestQueue(SatpamPeristiwaActivity.this);
        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                loading.dismiss();
                Log.d("LOG", "Response: " + response.toString());

                try {
                    JSONArray array_success = new JSONArray(response);
                    int size = array_success.length();
                    allItems.clear();
                    Peristiwa model;
                    for (int i = 0; i<size; i++){
                        JSONObject data = array_success.getJSONObject(i);
                        int id_peristiwa = Integer.parseInt(data.getString("id_peristiwa"));
                        Date tgl_peristiwa = DateHelper.stringToDate(data.getString("tgl_peristiwa"), new SimpleDateFormat("yyyy-MM-dd"));
                        String jam_peristiwa = data.getString("jam_peristiwa");
                        String kejadian = data.getString("kejadian");
                        String deskripsi = data.getString("deskripsi");
                        String video = data.getString("video");
                        String status_penyelesaian = data.getString("status_penyelesaian");
                        String username = data.getString("username");
                        String total = data.getString("total");
                        JSONArray array_saksi = data.getJSONArray("saksi");
                        ArrayList<String> saksi = new ArrayList<>();
                        for (int j = 0; j < array_saksi.length(); j++){
                            saksi.add(array_saksi.get(j).toString());
                        }
                        model = new Peristiwa();
                        model.setId_peristiwa(id_peristiwa);
                        model.setTgl_peristiwa(tgl_peristiwa);
                        model.setJam_peristiwa(jam_peristiwa);
                        model.setKejadian(kejadian);
                        model.setDeskripsi(deskripsi);
                        model.setVideo(video);
                        model.setStatus_penyelesaian(status_penyelesaian);
                        model.setUsername(username);
                        model.setTotal(total);
                        model.setSaksi(saksi);
                        String gambar = data.getString("gambar");
                        model.setFoto(gambar);
                        allItems.add(model);
                        adapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                loading.dismiss();
                Log.e("ERROR", "Error: " + error.getMessage());
                Toast.makeText(SatpamPeristiwaActivity.this,
                        "Gagal Menampilkan Data.", Toast.LENGTH_LONG).show();
            }
        });

        // Adding request to request queue
        queue.add(strReq);

        return allItems;
    }
}