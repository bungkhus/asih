package com.bungkhus.ministudio.asih.view.tatausaha.activity;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.helper.DateHelper;
import com.bungkhus.ministudio.asih.helper.MenambahMengurangTanggal;
import com.bungkhus.ministudio.asih.helper.SessionManager;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

public class KatuAddJadwalPegawaiActivity extends AppCompatActivity {

    private HashMap<String, String> user;
    private String username, status, nama, title, sebagai, url, un_ybs;
    private SessionManager session;
    private CatLoadingView loading;
    private Spinner spSatpam, spCaraka;
    private EditText etTglAwal, etTglAkhir;
    private Button btSimpan;
    final Calendar c_mulai = Calendar.getInstance();
    final Calendar c_selesai = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_katu_add_jadwal_pegawai);
        inisialisasiKomponen();
        setupActionBar();
        setActionClick();
    }

    private void inisialisasiKomponen(){
        session = new SessionManager(getApplicationContext());
        loading = new CatLoadingView();
        user = session.getData();
        username = user.get(SessionManager.KEY_USERNAME);
        status = user.get(SessionManager.KEY_STATUS);
        nama = user.get(SessionManager.KEY_NAMA);
        title = getIntent().getStringExtra("title");
        sebagai = getIntent().getStringExtra("sebagai");
        un_ybs = getIntent().getStringExtra("un_ybs");
        url = getResources().getString(R.string.ipaddr)+"act=setJadwal";
        spCaraka = (Spinner) findViewById(R.id.spCaraka);
        spSatpam = (Spinner) findViewById(R.id.spTugas);
        etTglAwal = (EditText) findViewById(R.id.etTglAwal);
        etTglAkhir = (EditText) findViewById(R.id.etTglAkhir);
        btSimpan = (Button) findViewById(R.id.btSimpan);
        if(sebagai.equalsIgnoreCase("caraka")){
            spCaraka.setVisibility(View.VISIBLE);
            spSatpam.setVisibility(View.GONE);
        }else{
            spCaraka.setVisibility(View.GONE);
            spSatpam.setVisibility(View.VISIBLE);
        }
    }

    private boolean isInputNotNull(){
        return etTglAwal.getText().length() > 0 &&
                etTglAkhir.getText().length() > 0;
    }

    private void showDatePicker(){
        DatePickerDialog d = new DatePickerDialog(KatuAddJadwalPegawaiActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar c2 = Calendar.getInstance();
                c2.set(year, monthOfYear, dayOfMonth);
                Calendar c_akhir = MenambahMengurangTanggal.tambahWaktu(c2.getTime(), 5, "hari");
                Calendar c_awal = MenambahMengurangTanggal.tambahWaktu(c2.getTime(), 0, "hari");
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                setJam(c_akhir, c_awal);

                boolean adaMinggu = false;

                for (Date date = c_awal.getTime(); date.before(c_akhir.getTime()); date = MenambahMengurangTanggal.tambahWaktu(date, 1, "hari").getTime())
                {
                    System.out.println(date);
                    System.out.println(adaMinggu);
                    if(DateHelper.dateToString(date, new SimpleDateFormat("EEEE")).equalsIgnoreCase("Sunday")){
                        adaMinggu = true;
                    }
                }

                if(c_awal.getTime().before(new Date())){
                    etTglAkhir.setText("");
                    etTglAwal.setText("");
                    Toast.makeText(KatuAddJadwalPegawaiActivity.this, "Tidak boleh pilih hari kemarin!", Toast.LENGTH_LONG).show();
                }else if(adaMinggu){
                    etTglAkhir.setText("");
                    etTglAwal.setText("");
                    Toast.makeText(KatuAddJadwalPegawaiActivity.this, "Tidak boleh pilih hari Minggu!", Toast.LENGTH_LONG).show();
                }else{
                    etTglAkhir.setText(dateFormat.format(c_akhir.getTime()));
                    etTglAwal.setText(dateFormat.format(c_awal.getTime()));
                }
            }
        }, c_mulai.get(Calendar.YEAR), c_mulai.get(Calendar.MONTH), c_mulai.get(Calendar.DAY_OF_MONTH));
        d.show();
    }

    private void setJam(Calendar c_mulai, Calendar c_selesai){
        if(getTugas().equalsIgnoreCase("Satpam Pagi")){
            c_mulai.set(Calendar.HOUR, 18);
            c_mulai.set(Calendar.SECOND, 0);
            c_mulai.set(Calendar.MINUTE, 0);
            c_selesai.set(Calendar.HOUR, 6);
            c_selesai.set(Calendar.MINUTE, 0);
            c_selesai.set(Calendar.SECOND, 0);
        }else if(getTugas().equalsIgnoreCase("Satpam Malam")){
            c_mulai.set(Calendar.HOUR, 6);
            c_mulai.set(Calendar.SECOND, 0);
            c_mulai.set(Calendar.MINUTE, 0);
            c_selesai.set(Calendar.HOUR, 18);
            c_selesai.set(Calendar.MINUTE, 0);
            c_selesai.set(Calendar.SECOND, 0);
        }else{
            c_mulai.set(Calendar.HOUR, 11);
            c_mulai.set(Calendar.SECOND, 59);
            c_mulai.set(Calendar.MINUTE, 59);
            c_selesai.set(Calendar.HOUR, 0);
            c_selesai.set(Calendar.MINUTE, 0);
            c_selesai.set(Calendar.SECOND, 0);
        }
    }

    private String getTugas(){
        String tugas;
        if(sebagai.equalsIgnoreCase("caraka")){
            tugas = spCaraka.getSelectedItem().toString();
        }else{
            tugas = spSatpam.getSelectedItem().toString();
        }
        return tugas;
    }

    private void setActionClick(){
        btSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isInputNotNull()){
                    postData(etTglAwal.getText().toString(), etTglAkhir.getText().toString(), getTugas());
                }else{
                    Toast.makeText(KatuAddJadwalPegawaiActivity.this, "Lengkapi Data!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        etTglAwal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Show DatePicker");
                showDatePicker();
            }
        });

        spSatpam.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                refreshForm();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spCaraka.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                refreshForm();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void refreshForm(){
        etTglAwal.setText("");
        etTglAkhir.setText("");
    }

    private void postData(final String tgl_awal, final String tgl_akhir, final String tugas){
        RequestQueue queue = Volley.newRequestQueue(KatuAddJadwalPegawaiActivity.this);
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("success");

                    // Check for error node in json
                    if (status.equalsIgnoreCase("1")) {
                        finish();
                        Toast.makeText(KatuAddJadwalPegawaiActivity.this,
                                "Jadwal berhasil terkirim!", Toast.LENGTH_LONG).show();
                    } else {
                        // Error in login. Get the error message
                        Toast.makeText(KatuAddJadwalPegawaiActivity.this,
                                "Gagal mengirim data!", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", "Submit Error: " + error.getMessage());
                Toast.makeText(KatuAddJadwalPegawaiActivity.this,
                        "Submit Failed", Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("act", "setJadwal");
                params.put("tglmulai", tgl_awal);
                params.put("tglselesai", tgl_akhir);
                params.put("username", un_ybs);
                params.put("tugas", tugas);
                return params;
            }

        };

        // Adding request to request queue
        queue.add(strReq);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_no_animation, R.anim.push_out_right);
    }

    private void setupActionBar(){
        getSupportActionBar().setElevation(3);
        getSupportActionBar().setTitle(title.toUpperCase());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

    }
}
