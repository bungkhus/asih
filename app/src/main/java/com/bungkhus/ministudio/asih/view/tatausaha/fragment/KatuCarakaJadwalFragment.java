package com.bungkhus.ministudio.asih.view.tatausaha.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.adapter.tatausaha.KatuPegawaiAdapter;
import com.bungkhus.ministudio.asih.helper.DateHelper;
import com.bungkhus.ministudio.asih.model.Pegawai;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class KatuCarakaJadwalFragment extends Fragment {

    private GridLayoutManager lLayout;
    private KatuPegawaiAdapter adapter;
    private CatLoadingView loading;
    private ImageView imgNoData;

    public KatuCarakaJadwalFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_katu_caraka_jadwal, container, false);

        imgNoData = (ImageView) view.findViewById(R.id.imgNoData);
        loading = new CatLoadingView();

        String url = getResources().getString(R.string.ipaddr)+"act=getAllPegawaiCaraka";
        List<Pegawai> rowListItem = getAllItemList(url);
        lLayout = new GridLayoutManager(getContext(), 3);

        RecyclerView rView = (RecyclerView)view.findViewById(R.id.recycler_view);
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);

        adapter = new KatuPegawaiAdapter(getContext(), rowListItem, new KatuPegawaiAdapter.OnItemClickListener() {
            @Override
            public void onClick(Pegawai item) {

            }
        });
        rView.setAdapter(adapter);

        return view;
    }

    private List<Pegawai> getAllItemList(final String url){

        final List<Pegawai> allItems = new ArrayList<Pegawai>();

        loading.show(getFragmentManager(), "");

        RequestQueue queue = Volley.newRequestQueue(getContext());
        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                loading.dismiss();
                Log.d("LOG", "Response: " + response.toString());

                try {
                    JSONArray array_success = new JSONArray(response);
                    int size = array_success.length();
                    allItems.clear();
                    Pegawai model = new Pegawai();
                    if(size > 0) {
                        imgNoData.setVisibility(View.GONE);
                        for (int i = 0; i<size; i++){
                            JSONObject data = array_success.getJSONObject(i);
                            String id_pegawai = data.getString("id_pegawai");
                            String nama = data.getString("nama");
                            Date tgl_lahir = DateHelper.stringToDate(data.getString("tgl_lahir"), new SimpleDateFormat("yyyy-MM-dd"));
                            String alamat = data.getString("alamat");
                            String no_hp = data.getString("no_hp");
                            String bagian = data.getString("bagian");
                            String foto = data.getString("foto");
                            String username = data.getString("username");

                            model = new Pegawai(id_pegawai, nama, tgl_lahir, alamat, no_hp, bagian, foto, username);
                            allItems.add(model);
                            adapter.notifyDataSetChanged();
                        }
                    }else{
                        imgNoData.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    // JSON error
                    imgNoData.setVisibility(View.VISIBLE);
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                imgNoData.setVisibility(View.VISIBLE);
                Log.e("ERROR", "Error: " + error.getMessage());
                Toast.makeText(getContext(),
                        "Gagal Menampilkan Data.", Toast.LENGTH_LONG).show();
            }
        });

        // Adding request to request queue
        queue.add(strReq);

        return allItems;
    }

}