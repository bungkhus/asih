package com.bungkhus.ministudio.asih.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.HashMap;

public class SessionManager {
    private static String TAG = SessionManager.class.getSimpleName();
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "Login";
    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_STATUS = "status";
    public static final String KEY_NAMA = "nama";
    public static final String KEY_ID_PEGAWAI = "id_pegawai";

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setData(String username, String nama, String status, String id_pegawai){
        editor.putString(KEY_NAMA, nama);
        editor.putString(KEY_USERNAME, username);
        editor.putString(KEY_STATUS, status);
        editor.putString(KEY_ID_PEGAWAI, id_pegawai);
        editor.commit();
    }

    public HashMap<String, String> getData(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_STATUS, pref.getString(KEY_STATUS, null));
        user.put(KEY_USERNAME, pref.getString(KEY_USERNAME, null));
        user.put(KEY_NAMA, pref.getString(KEY_NAMA, null));
        user.put(KEY_ID_PEGAWAI, pref.getString(KEY_ID_PEGAWAI, null));
        return user;
    }

    public void setLogin(boolean isLoggedIn) {
        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);
        editor.commit();
        Log.d(TAG, "User login session modified!");
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }
}
