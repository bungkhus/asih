package com.bungkhus.ministudio.asih.view;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.helper.SessionManager;
import com.bungkhus.ministudio.asih.view.caraka.activity.CarakaMainActivity;
import com.bungkhus.ministudio.asih.view.satpam.activity.SatpamMainActivity;
import com.bungkhus.ministudio.asih.view.tatausaha.activity.KatuMainActivity;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    EditText etUsername;
    EditText etPassword;
    FloatingActionButton btFab;

    private SessionManager session;
    private CatLoadingView loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        session = new SessionManager(LoginActivity.this);
        loading = new CatLoadingView();
        etUsername = (EditText) findViewById(R.id.et_username);
        etPassword = (EditText) findViewById(R.id.et_password);
        btFab = (FloatingActionButton) findViewById(R.id.fab);

////        session.setLogin(false);
//        if (session.isLoggedIn()) {
//            HashMap<String, String> user = session.getData();
//            final String akses = user.get(SessionManager.KEY_STATUS);
//
//            if(akses.equalsIgnoreCase("KaTu")){
//                Intent intent = new Intent(LoginActivity.this, KatuMainActivity.class);
//                startActivity(intent);
//                finish();
//            }else if(akses.equalsIgnoreCase("satpam")){
//                Intent intent = new Intent(LoginActivity.this, SatpamMainActivity.class);
//                startActivity(intent);
//                finish();
//            }else if(akses.equalsIgnoreCase("caraka")){
//                Intent intent = new Intent(LoginActivity.this, CarakaMainActivity.class);
//                startActivity(intent);
//                finish();
//            }else{
//                Toast.makeText(LoginActivity.this, "Anda tidak memiliki akses ke aplikasi!", Toast.LENGTH_LONG).show();
//            }
//        }

        btFab.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab:
                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();
                String url = getResources().getString(R.string.ipaddr)+"act=login&username="+username+"&password="+password;
                if(etUsername.length() > 0 && etPassword.length() > 0){
                    getData(url, LoginActivity.this);
                }else{
                    Toast.makeText(LoginActivity.this, "Tidak boleh ada fiel yang kosong!", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public void getData(final String url, final Context context){
        loading.show(getSupportFragmentManager(), "");
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            ;

            @Override
            public void onResponse(String response) {
                Log.d("Interactor Response", response);
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("login_success");

                    if(status.equalsIgnoreCase("0")) {
                        loading.dismiss();
                        Toast.makeText(getApplicationContext(), "Please Check Your Credential!", Toast.LENGTH_LONG).show();
                    } else {
                        String json_akses = jObj.getString("login_status");
                        String json_nama = jObj.getString("login_nama");
                        String json_un = jObj.getString("login_username");
                        String id_pegawai = jObj.getString("id_pegawai");
                        session.setLogin(true);
                        session.setData(json_un, json_nama, json_akses, id_pegawai);

                        // Launch main activity
                        if(json_akses.equalsIgnoreCase("KaTu")){
                            Intent intent = new Intent(LoginActivity.this, KatuMainActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                            finish();
                        }else if(json_akses.equalsIgnoreCase("satpam")){
                            Intent intent = new Intent(LoginActivity.this, SatpamMainActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                            finish();
                        }else if(json_akses.equalsIgnoreCase("caraka")){
                            Intent intent = new Intent(LoginActivity.this, CarakaMainActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                            finish();
                        }else{
                            Toast.makeText(getApplicationContext(), "Maaf, Anda tidak memiliki hak akses untuk masuk ke dalam sistem.", Toast.LENGTH_LONG).show();
                        }
                        loading.dismiss();
                    }
                } catch (JSONException e) {
                    loading.dismiss();
                    Toast.makeText(getApplicationContext(), "Parsing JSON Error!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                Toast.makeText(context, "Check your Internet connection", Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(stringRequest);
    }
}
