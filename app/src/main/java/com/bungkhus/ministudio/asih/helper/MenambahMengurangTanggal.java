package com.bungkhus.ministudio.asih.helper;

/**
 * Created by bungkhus on 6/27/16.
 */
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/*
 *  @Author Firman Hidayat
 */

public class MenambahMengurangTanggal {
    // Method menambah dan mengurangi waktu
    public static Calendar tambahWaktu(Date waktuPermulaan,
                                       int jmlTambahanWaktu, String satuan) {
        /*
         * Untuk mengurangi hari gunakan nilai minus (-) pada jmlTambahanWaktu
         */
        Calendar cal = Calendar.getInstance();
        cal.setTime(waktuPermulaan);
        switch (satuan) {
            case "hari":
                cal.add(Calendar.DATE, jmlTambahanWaktu);
                break;
            case "bulan":
                cal.add(Calendar.MONTH, jmlTambahanWaktu);
                break;
            case "tahun":
                cal.add(Calendar.YEAR, jmlTambahanWaktu);
                break;
            case "jam":
                cal.add(Calendar.HOUR, jmlTambahanWaktu);
                break;
            case "menit":
                cal.add(Calendar.MINUTE, jmlTambahanWaktu);
                break;
            case "detik":
                cal.add(Calendar.SECOND, jmlTambahanWaktu);
                break;
            case "milidetik":
                cal.add(Calendar.MILLISECOND, jmlTambahanWaktu);
                break;
        }
        return cal;
    }

    // Menampilkan Date terformat
    public static String tampilkanTanggalDanWaktu(Date tanggalDanWaktu,
                                                  String pola, Locale lokal) {
        String tanggalStr;
        SimpleDateFormat formatter;
        if (lokal == null) {
            formatter = new SimpleDateFormat(pola);
        } else {
            formatter = new SimpleDateFormat(pola, lokal);
        }
        tanggalStr = formatter.format(tanggalDanWaktu);
        return tanggalStr;
    }
}