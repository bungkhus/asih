package com.bungkhus.ministudio.asih.adapter.caraka;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.helper.DateHelper;
import com.bungkhus.ministudio.asih.model.Jadwal;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by ahlul on 07/08/2016.
 */
public class CarakaJadwalAdapter extends RecyclerView.Adapter<CarakaJadwalAdapter.RecyclerViewHolders> {

    private final OnItemClickListener listener;
    private List<Jadwal> itemList;
    private Context context;
    private String sebagai;

    public CarakaJadwalAdapter(Context context, List<Jadwal> itemList, OnItemClickListener listener, String sebagai) {
        this.itemList = itemList;
        this.context = context;
        this.listener = listener;
        this.sebagai = sebagai;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_caraka_jadwal, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        holder.click(itemList.get(position), listener);
        SimpleDateFormat dateFormat;
        if(sebagai.equalsIgnoreCase("satpam")){
            dateFormat = new SimpleDateFormat("dd MMMM yyyy, HH:mm");
        }else{
            dateFormat = new SimpleDateFormat("dd MMMM yyyy");
        }
        String tanggal = DateHelper.dateToString(itemList.get(position).getTgl_mulai(), dateFormat)+" s.d.\n"+DateHelper.dateToString(itemList.get(position).getTgl_selesai(), dateFormat);
        holder.tvTanggal.setText(tanggal);
        holder.tvTugas.setText(itemList.get(position).getTugas());
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView tvTugas, tvTanggal;


        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvTugas = (TextView)itemView.findViewById(R.id.tvTugas);
            tvTanggal = (TextView)itemView.findViewById(R.id.tvTanggal);
        }

        public void click(final Jadwal item, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(item);
                }
            });
        }

        @Override
        public void onClick(View view) {

        }
    }

    public interface OnItemClickListener {
        void onClick(Jadwal item);
    }
}