package com.bungkhus.ministudio.asih.adapter.satpam;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.model.Penilaian;

import java.util.List;

/**
 * Created by ahlul on 07/08/2016.
 */
public class SatpamPenilaianAdapter extends RecyclerView.Adapter<SatpamPenilaianAdapter.RecyclerViewHolders> {

    private final OnItemClickListener listener;
    private List<Penilaian> itemList;
    private Context context;

    public SatpamPenilaianAdapter(Context context, List<Penilaian> itemList, OnItemClickListener listener) {
        this.itemList = itemList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_satpam_penilaian, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        holder.click(itemList.get(position), listener);
        holder.tvAbsensi.setText("Absensi : "+itemList.get(position).getAbsensi());
        holder.tvKesigapan.setText("Kesigapan : "+itemList.get(position).getKesigapan());
        holder.tvKerajinan.setText("Kerajinan : "+itemList.get(position).getKerajinan());
        holder.tvKebersihan.setText("Kebersihan : "+itemList.get(position).getKebersihan());
        holder.tvTotal.setText("Total : "+itemList.get(position).getNilai_akhir());
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView tvAbsensi, tvKesigapan, tvKerajinan, tvKebersihan, tvTotal;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvAbsensi = (TextView)itemView.findViewById(R.id.tvAbsensi);
            tvKesigapan = (TextView)itemView.findViewById(R.id.tvKesigapan);
            tvKerajinan = (TextView)itemView.findViewById(R.id.tvKerajinan);
            tvKebersihan = (TextView)itemView.findViewById(R.id.tvKebersihan);
            tvTotal = (TextView)itemView.findViewById(R.id.tvTotal);
        }

        public void click(final Penilaian item, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(item);
                }
            });
        }

        @Override
        public void onClick(View view) {

        }
    }

    public interface OnItemClickListener {
        void onClick(Penilaian item);
    }
}

