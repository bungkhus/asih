package com.bungkhus.ministudio.asih.adapter.caraka;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.helper.DateHelper;
import com.bungkhus.ministudio.asih.model.PengajuanAlat;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by ahlul on 07/08/2016.
 */
public class CarakaPengajuanAlatAdapter extends RecyclerView.Adapter<CarakaPengajuanAlatAdapter.RecyclerViewHolders> {

    private final OnItemClickListener listener;
    private List<PengajuanAlat> itemList;
    private Context context;
    private String sebagai;

    public CarakaPengajuanAlatAdapter(Context context, List<PengajuanAlat> itemList, OnItemClickListener listener, String sebagai) {
        this.itemList = itemList;
        this.context = context;
        this.listener = listener;
        this.sebagai = sebagai;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_caraka_pengajuanalat, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        holder.click(itemList.get(position), listener);
        holder.tvTanggal.setText(DateHelper.dateToString(itemList.get(position).getTgl_pengajuan(), new SimpleDateFormat("dd MMM yyyy")));
        holder.tvBarang.setText(itemList.get(position).getJumlah_barang()+" "+itemList.get(position).getNama_barang()+" "+itemList.get(position).getMerk_barang());
        if(sebagai.equalsIgnoreCase("caraka") || sebagai.equalsIgnoreCase("satpam")){
            holder.button.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView tvTanggal, tvBarang, tvMerk, tvJumlah, button;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvTanggal = (TextView)itemView.findViewById(R.id.tvTanggal);
            tvBarang =(TextView)itemView.findViewById(R.id.tvBarang);
            tvMerk=(TextView)itemView.findViewById(R.id.tvMerk);
            tvJumlah=(TextView)itemView.findViewById(R.id.tvJumlah);
            button=(TextView)itemView.findViewById(R.id.button);
        }

        public void click(final PengajuanAlat item, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(item);
                }
            });
        }

        @Override
        public void onClick(View view) {

        }
    }

    public interface OnItemClickListener {
        void onClick(PengajuanAlat item);
    }
}
