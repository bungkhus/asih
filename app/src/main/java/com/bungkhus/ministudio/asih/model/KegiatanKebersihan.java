package com.bungkhus.ministudio.asih.model;

import java.util.Date;

/**
 * Created by ahlul on 07/08/2016.
 */
public class KegiatanKebersihan {

    String nama_kegiatan, keterangan;
    Date waktu;

    public KegiatanKebersihan() {
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getNama_kegiatan() {
        return nama_kegiatan;
    }

    public void setNama_kegiatan(String nama_kegiatan) {
        this.nama_kegiatan = nama_kegiatan;
    }

    public Date getWaktu() {
        return waktu;
    }

    public void setWaktu(Date waktu) {
        this.waktu = waktu;
    }
}
