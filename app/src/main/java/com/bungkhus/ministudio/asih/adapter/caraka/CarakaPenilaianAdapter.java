package com.bungkhus.ministudio.asih.adapter.caraka;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.helper.DateHelper;
import com.bungkhus.ministudio.asih.helper.TextDrawable;
import com.bungkhus.ministudio.asih.model.Penilaian;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by ahlul on 07/08/2016.
 */
public class CarakaPenilaianAdapter extends RecyclerView.Adapter<CarakaPenilaianAdapter.RecyclerViewHolders> {

    private final OnItemClickListener listener;
    private List<Penilaian> itemList;
    private Context context;
    private String sebagai;

    public CarakaPenilaianAdapter(Context context, List<Penilaian> itemList, OnItemClickListener listener, String sebagai) {
        this.itemList = itemList;
        this.context = context;
        this.listener = listener;
        this.sebagai = sebagai;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_caraka_penilaian, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        holder.click(itemList.get(position), listener);
        holder.tvAbsensi.setText("Absensi : "+itemList.get(position).getAbsensi());
        holder.tvKesigapan.setText("Kesigapan : "+itemList.get(position).getKesigapan());
        holder.tvKeramahan.setText("Keramahan : "+itemList.get(position).getKeramahan());

        if(sebagai.equalsIgnoreCase("caraka")){
            holder.tvKerajinan.setText("Kerajinan : "+itemList.get(position).getKerajinan());
            holder.tvKebersihan.setText("Kebersihan : "+itemList.get(position).getKebersihan());
        }else{
            holder.tvKerajinan.setText("Patroli : "+itemList.get(position).getPatroli());
            holder.tvKebersihan.setText("Layanan Tamu : "+itemList.get(position).getLayanan_tamu());
        }

        DecimalFormat formatter = new DecimalFormat("#.0");
        String numberAsString = formatter.format(itemList.get(position).getNilai_akhir());
        holder.tvTotal.setText(numberAsString);

        Date bulanDt = DateHelper.stringToDate(itemList.get(position).getPeriode(), new SimpleDateFormat("yyyy-MM"));
        String bulanStr = DateHelper.dateToString(bulanDt, new SimpleDateFormat("MMMM"));
        holder.tvBulan.setText(bulanStr);
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView tvAbsensi, tvKesigapan, tvKerajinan, tvKebersihan, tvTotal, tvKeramahan, tvBulan;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvAbsensi = (TextView)itemView.findViewById(R.id.tvAbsensi);
            tvKesigapan = (TextView)itemView.findViewById(R.id.tvKesigapan);
            tvKeramahan = (TextView)itemView.findViewById(R.id.tvKeramahan);
            tvKerajinan = (TextView)itemView.findViewById(R.id.tvKerajinan);
            tvKebersihan = (TextView)itemView.findViewById(R.id.tvKebersihan);
            tvTotal = (TextView)itemView.findViewById(R.id.tvTotal);
            tvBulan = (TextView)itemView.findViewById(R.id.tvBulan);
        }

        public void click(final Penilaian item, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(item);
                }
            });
        }

        @Override
        public void onClick(View view) {

        }
    }

    public interface OnItemClickListener {
        void onClick(Penilaian item);
    }
}
