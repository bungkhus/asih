package com.bungkhus.ministudio.asih.view.tatausaha.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.adapter.tatausaha.KatuPegawaiAdapter;
import com.bungkhus.ministudio.asih.helper.DateHelper;
import com.bungkhus.ministudio.asih.model.Pegawai;
import com.bungkhus.ministudio.asih.view.tatausaha.activity.KatuAddJadwalPegawaiActivity;
import com.bungkhus.ministudio.asih.view.tatausaha.activity.KatuAddPegawaiActivity;
import com.bungkhus.ministudio.asih.view.tatausaha.activity.KatuDetailPegawaiActivity;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class KatuSatpamPegawaiFragment extends Fragment {

    private GridLayoutManager lLayout;
    private KatuPegawaiAdapter adapter;
    private CatLoadingView loading;
    private String url;
    private List<Pegawai> rowListItem;
    private ImageView imgNoData;


    public KatuSatpamPegawaiFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_katu_satpam_pegawai, container, false);
        setHasOptionsMenu(true);
        loading = new CatLoadingView();
        imgNoData = (ImageView) view.findViewById(R.id.imgNoData);

        url = getResources().getString(R.string.ipaddr)+"act=getAllPegawaiSatpam";
        rowListItem = getAllItemList(url);
        lLayout = new GridLayoutManager(getContext(), 3);

        RecyclerView rView = (RecyclerView)view.findViewById(R.id.recycler_view);
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);

        adapter = new KatuPegawaiAdapter(getContext(), rowListItem, new KatuPegawaiAdapter.OnItemClickListener() {
            @Override
            public void onClick(Pegawai item) {
                goTo(KatuDetailPegawaiActivity.class, item.getNama().toUpperCase(), item.getUsername(), item.getId_pegawai());
            }
        });
        rView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.add, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub

        switch (item.getItemId()) {

            case R.id.action_add:
                goToAdd(KatuAddPegawaiActivity.class, "TAMBAH "+getActivity().getIntent().getStringExtra("title")+" SATPAM", "-");
                break;
        }
        return true;
    }

    void goToAdd(Class<?> cls, String title, String un_ybs){
        Intent i = new Intent(getContext(), cls);
        i.putExtra("title", title);
        i.putExtra("sebagai", "satpam");
        i.putExtra("un_ybs", un_ybs);
        startActivityForResult(i, 313);
        getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.activity_no_animation);
    }

    void goTo(Class<?> cls, String title, String un_ybs, String id){
        Intent i = new Intent(getContext(), cls);
        i.putExtra("title", title);
        i.putExtra("sebagai", "satpam");
        i.putExtra("un_ybs", un_ybs);
        i.putExtra("id_pegawai", id);
        startActivity(i);
        getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.activity_no_animation);
    }

    private List<Pegawai> getAllItemList(final String url){

        final List<Pegawai> allItems = new ArrayList<Pegawai>();

        loading.show(getFragmentManager(), "");

        RequestQueue queue = Volley.newRequestQueue(getContext());
        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                loading.dismiss();
                Log.d("LOG SATPAM", "Response: " + response.toString());

                try {
                    JSONArray array_success = new JSONArray(response);
                    int size = array_success.length();
                    allItems.clear();
                    Pegawai model = new Pegawai();
                    if(size == 0 || response.equalsIgnoreCase("[]")){
                        imgNoData.setVisibility(View.VISIBLE);
                    }else{
                        imgNoData.setVisibility(View.GONE);
                        for (int i = 0; i<size; i++){
                            JSONObject data = array_success.getJSONObject(i);
                            String id_pegawai = data.getString("id_pegawai");
                            String nama = data.getString("nama");
                            Date tgl_lahir = DateHelper.stringToDate(data.getString("tgl_lahir"), new SimpleDateFormat("yyyy-MM-dd"));
                            String alamat = data.getString("alamat");
                            String no_hp = data.getString("no_hp");
                            String bagian = data.getString("bagian");
                            String foto = data.getString("foto");
                            String username = data.getString("username");

                            model = new Pegawai(id_pegawai, nama, tgl_lahir, alamat, no_hp, bagian, foto, username);
                            allItems.add(model);
                            adapter.notifyDataSetChanged();
                        }
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                Log.e("ERROR", "Error: " + error.getMessage());
                Toast.makeText(getContext(),
                        "Gagal Menampilkan Data.", Toast.LENGTH_LONG).show();
            }
        });

        // Adding request to request queue
        queue.add(strReq);

        return allItems;
    }

    // Get Result Back
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("onActivityResult", "requestCode = " + requestCode);
        if(requestCode == 313){
            rowListItem = getAllItemList(url);
            adapter.notifyDataSetChanged();
        }
    }

}
