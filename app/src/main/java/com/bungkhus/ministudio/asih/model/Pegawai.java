package com.bungkhus.ministudio.asih.model;

import java.util.Date;

/**
 * Created by bungkhus on 8/7/16.
 */
public class Pegawai {

    private String id_pegawai;
    private String nama;
    private Date tgl_lahir;
    private String alamat;
    private String no_hp;
    private String bagian;
    private String foto;
    private String username;

    public Pegawai() {
    }

    public Pegawai(String id_pegawai, String nama, Date tgl_lahir, String alamat, String no_hp, String bagian, String foto, String username) {
        this.id_pegawai = id_pegawai;
        this.nama = nama;
        this.tgl_lahir = tgl_lahir;
        this.alamat = alamat;
        this.no_hp = no_hp;
        this.bagian = bagian;
        this.foto = foto;
        this.username = username;
    }

    public String getId_pegawai() {
        return id_pegawai;
    }

    public void setId_pegawai(String id_pegawai) {
        this.id_pegawai = id_pegawai;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Date getTgl_lahir() {
        return tgl_lahir;
    }

    public void setTgl_lahir(Date tgl_lahir) {
        this.tgl_lahir = tgl_lahir;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNo_hp() {
        return no_hp;
    }

    public void setNo_hp(String no_hp) {
        this.no_hp = no_hp;
    }

    public String getBagian() {
        return bagian;
    }

    public void setBagian(String bagian) {
        this.bagian = bagian;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
