package com.bungkhus.ministudio.asih.view.tatausaha.activity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.helper.SessionManager;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class KatuAddNilaiPegawaiActivity extends AppCompatActivity {

    Spinner listBulanPeriode, listTahunPeriode, spinnerKesigapan, spinnerPatroli, spinnerLayananTamu, spinnerKeramahan, spinnerKerajinan, spinnerKebersihan;
    EditText inAbsensi;
    TextView tPatroli, tLayananTamu, tKerajinan, tKebersihan;
    Button bInputPenilaian;
    String periodeFinal, absensiFinal, bulan, tahun,
            kesigapan, patroli, layanan_tamu, keramahan, kerajinan, kebersihan;
    String response, aksi, un_ybs, url_get_absen, bln, thn;

    private HashMap<String, String> user;
    private String username, status, nama, title, id_pegawai, sebagai;
    private SessionManager session;
    private CatLoadingView loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_katu_add_nilai_pegawai);
        session = new SessionManager(getApplicationContext());
        loading = new CatLoadingView();
        user = session.getData();
        username = user.get(SessionManager.KEY_USERNAME);
        status = user.get(SessionManager.KEY_STATUS);
        nama = user.get(SessionManager.KEY_NAMA);
        title = getIntent().getStringExtra("title");
        id_pegawai = getIntent().getStringExtra("id_pegawai");
        sebagai = getIntent().getStringExtra("sebagai");
        un_ybs = getIntent().getStringExtra("un_ybs");
        setupActionBar();
        setupView();
        setStatusPegawai(sebagai);
        setAbsensi();
    }

    private void setStatusPegawai(String status){
        if(status.equalsIgnoreCase("satpam")){
            tPatroli.setVisibility(View.VISIBLE);
            tLayananTamu.setVisibility(View.VISIBLE);
            tKebersihan.setVisibility(View.GONE);
            tKerajinan.setVisibility(View.GONE);
            spinnerPatroli.setVisibility(View.VISIBLE);
            spinnerLayananTamu.setVisibility(View.VISIBLE);
            spinnerKebersihan.setVisibility(View.GONE);
            spinnerKerajinan.setVisibility(View.GONE);
        }else{
            tPatroli.setVisibility(View.GONE);
            tLayananTamu.setVisibility(View.GONE);
            tKebersihan.setVisibility(View.VISIBLE);
            tKerajinan.setVisibility(View.VISIBLE);
            spinnerPatroli.setVisibility(View.GONE);
            spinnerLayananTamu.setVisibility(View.GONE);
            spinnerKebersihan.setVisibility(View.VISIBLE);
            spinnerKerajinan.setVisibility(View.VISIBLE);
        }
    }

    private void setAbsensi(){
        listBulanPeriode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                aksi = "get_absensi";
                url_get_absen = getResources().getString(R.string.ipaddr)+"act=getAbsensiPersenByUsernameAndPeriode";
                bln = String.valueOf(adapterView.getSelectedItemPosition()+1);
                thn = listTahunPeriode.getSelectedItem().toString();
                new ConnectionHelper().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        listTahunPeriode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                aksi = "get_absensi";
                url_get_absen = getResources().getString(R.string.ipaddr)+"act=getAbsensiPersenByUsernameAndPeriode";
                bln = String.valueOf(listBulanPeriode.getSelectedItemPosition()+1);
                thn = adapterView.getSelectedItem().toString();
                new ConnectionHelper().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setupView(){
        listBulanPeriode = (Spinner) findViewById(R.id.listBulanPeriode);
        listTahunPeriode = (Spinner) findViewById(R.id.listTahunPeriode);
        inAbsensi = (EditText) findViewById(R.id.inAbsensi);
        bInputPenilaian = (Button) findViewById(R.id.bInputNilai);
        spinnerKesigapan = (Spinner) findViewById(R.id.spinnerKesigapan);
        tPatroli = (TextView) findViewById(R.id.tPatroli);
        tLayananTamu = (TextView) findViewById(R.id.tLayananTamu);
        tKerajinan = (TextView) findViewById(R.id.tKerajinan);
        tKebersihan = (TextView) findViewById(R.id.tKebersihan);
        spinnerPatroli = (Spinner) findViewById(R.id.spinnerPatroli);
        spinnerLayananTamu = (Spinner) findViewById(R.id.spinnerLayananTamu);
        spinnerKeramahan = (Spinner) findViewById(R.id.spinnerKeramahan);
        spinnerKerajinan = (Spinner) findViewById(R.id.spinnerKerajinan);
        spinnerKebersihan = (Spinner) findViewById(R.id.spinnerKebersihan);
        bInputPenilaian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bulan = String.valueOf(listBulanPeriode.getSelectedItemPosition() + 1);
                tahun = listTahunPeriode.getSelectedItem().toString();
                if (bulan.length() == 1) bulan = "0" + bulan;

                kesigapan = String.valueOf((spinnerKesigapan.getSelectedItemPosition()+1)*20);
                patroli = String.valueOf((spinnerPatroli.getSelectedItemPosition()+1)*20);
                layanan_tamu = String.valueOf((spinnerLayananTamu.getSelectedItemPosition()+1)*20);
                keramahan = String.valueOf((spinnerKeramahan.getSelectedItemPosition()+1)*20);
                kerajinan = String.valueOf((spinnerKerajinan.getSelectedItemPosition()+1)*20);
                kebersihan = String.valueOf((spinnerKebersihan.getSelectedItemPosition()+1)*20);
                periodeFinal = tahun + "-" + bulan;
                absensiFinal = inAbsensi.getText().toString();

                aksi = "set_penilaian";
                new ConnectionHelper().execute();
            }
        });
        final Calendar c = Calendar.getInstance();
        bulan = new SimpleDateFormat("MM").format(c.getTime());
        listBulanPeriode.setSelection((Integer.parseInt(bulan) - 1));

        tahun = new SimpleDateFormat("yyyy").format(c.getTime());
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.thn, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        listTahunPeriode.setAdapter(adapter);
        if (!tahun.equals(null)) {
            int spinnerPosition = adapter.getPosition(tahun);
            listTahunPeriode.setSelection(spinnerPosition);
        }
    }

    private class ConnectionHelper extends AsyncTask<String, Void, String> {
        ProgressDialog p = new ProgressDialog(KatuAddNilaiPegawaiActivity.this);
        @Override
        protected void onPreExecute() {
            response = "";
            p.setTitle("Tunggu sebentar...");
            p.show();
        }

        @Override
        protected String doInBackground(String... params) {
            URL url = null;
            try {
                List<Pair> paramss = new ArrayList<Pair>();
                if (aksi.equals("get_absensi")) {
                    url = new URL(url_get_absen);
                    paramss.add(new Pair("username", un_ybs));
                    paramss.add(new Pair("bln", bln));
                    paramss.add(new Pair("thn", thn));
                }else{
                    url = new URL(getResources().getString(R.string.ipaddr)+"act=setPenilaian");
                    double nilai;
                    if(sebagai.equalsIgnoreCase("satpam")) {
                        nilai = (Double.parseDouble(absensiFinal)+Integer.parseInt(kesigapan)+Integer.parseInt(keramahan)+Integer.parseInt(patroli)+Integer.parseInt(layanan_tamu))/5;
                    }else{
                        nilai = (Double.parseDouble(absensiFinal)+Integer.parseInt(kesigapan)+Integer.parseInt(keramahan)+Integer.parseInt(kebersihan)+Integer.parseInt(kerajinan))/5;
                    }
                    String nilai_akhir = String.valueOf(nilai);
                    paramss.add(new Pair("periode", periodeFinal));
                    paramss.add(new Pair("absensi", absensiFinal));
                    paramss.add(new Pair("id_pegawai", id_pegawai));
                    paramss.add(new Pair("kesigapan", kesigapan));
                    paramss.add(new Pair("patroli", patroli));
                    paramss.add(new Pair("layanan_tamu", layanan_tamu));
                    paramss.add(new Pair("keramahan", keramahan));
                    paramss.add(new Pair("kerajinan", kerajinan));
                    paramss.add(new Pair("kebersihan", kebersihan));
                    paramss.add(new Pair("nilai_akhir", nilai_akhir));
                }

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getQuery(paramss));
                writer.flush();
                writer.close();
                os.close();

                conn.connect();
                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line=br.readLine()) != null) {
                        response+=line;
                    }
                } else {
                    response = "";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            p.cancel();
            if (result != "") {
                Log.d("Set Log", result.replace("null", ""));
                try {
                    if (aksi.equals("get_absensi")) {
                        JSONArray json = new JSONArray(result.replace("null",""));
                        for(int i = 0;i < json.length(); i++) {
                            inAbsensi.setText(json.getJSONObject(i).getString("absensi"));
                        }
                    } else {
                        JSONObject json = new JSONObject(result.replace("null", ""));
                        if (json.getString("success").equals("1")) {
                            Toast.makeText(KatuAddNilaiPegawaiActivity.this, "Input berhasil", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(KatuAddNilaiPegawaiActivity.this, "Error", Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(KatuAddNilaiPegawaiActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(KatuAddNilaiPegawaiActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String getQuery(List<Pair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (Pair<String, String> pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.first, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.second, "UTF-8"));
        }

        return result.toString();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_no_animation, R.anim.push_out_right);
    }

    void setupActionBar(){
        getSupportActionBar().setElevation(3);
        getSupportActionBar().setTitle(title.toUpperCase());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

    }
}
