package com.bungkhus.ministudio.asih.view.satpam.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.helper.SessionManager;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class SatpamInputKunjunganActivity extends AppCompatActivity {

    private SessionManager session;
    private CatLoadingView loading;
    private HashMap<String, String> user;
    private String username, status, nama, title;

    EditText inTglKunjungan, inJamKunjungan, inNamaKunjungan, inInstitusiKunjungan, inAgendaKunjungan, inJumlahRombongan, inNoHpRombongan, inAlamatRombongan;
    Button bInputKunjungan;
    String tglFinal, jamFinal, namaFinal, instisusiFinal, agendaFinal, idPresensi, jumlahRombongan, noHp, alamat;
    SimpleDateFormat dateFormat, dateFormat2;
    String response, aksi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_satpam_input_kunjungan);
        session = new SessionManager(getApplicationContext());
        loading = new CatLoadingView();
        user = session.getData();
        username = user.get(SessionManager.KEY_USERNAME);
        status = user.get(SessionManager.KEY_STATUS);
        nama = user.get(SessionManager.KEY_NAMA);
        title = getIntent().getStringExtra("title");
        setupActionBar();
        setupComponent();
    }

    private void setupComponent(){
        inTglKunjungan = (EditText) findViewById(R.id.inTglKunjungan);
        inJamKunjungan = (EditText) findViewById(R.id.inJamKunjungan);
        inNamaKunjungan = (EditText) findViewById(R.id.inNamaKunjungan);
        inInstitusiKunjungan = (EditText) findViewById(R.id.inInstitusiKunjungan);
        inAgendaKunjungan = (EditText) findViewById(R.id.inAgendaKunjungan);
        inJumlahRombongan = (EditText) findViewById(R.id.inJumlahRombongan);
        inNoHpRombongan = (EditText) findViewById(R.id.inNoHpKunjungan);
        inAlamatRombongan = (EditText) findViewById(R.id.inAlamatKunjungan);
        bInputKunjungan = (Button) findViewById(R.id.bInputKunjungan);
        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat2 = new SimpleDateFormat("HH:mm");
        final Calendar c = Calendar.getInstance();
        tglFinal = dateFormat.format(c.getTime());
        jamFinal = dateFormat2.format(c.getTime());
        inTglKunjungan.setText(tglFinal);
        inJamKunjungan.setText(jamFinal);

        inTglKunjungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog d = new DatePickerDialog(SatpamInputKunjunganActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar c2 = Calendar.getInstance();
                        c2.set(year, monthOfYear, dayOfMonth);
                        tglFinal = dateFormat.format(c2.getTime());
                        inTglKunjungan.setText(tglFinal);
                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                d.show();
            }
        });
        inJamKunjungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog t = new TimePickerDialog(SatpamInputKunjunganActivity.this, new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        jamFinal = hourOfDay + ":" + minute;
                        inJamKunjungan.setText(jamFinal);
                    }
                }, c.get(Calendar.HOUR), c.get(Calendar.MINUTE), true);
                t.show();
            }
        });
        bInputKunjungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                namaFinal = inNamaKunjungan.getText().toString().trim();
                instisusiFinal = inInstitusiKunjungan.getText().toString().trim();
                agendaFinal = inAgendaKunjungan.getText().toString().trim();
                jumlahRombongan = inJumlahRombongan.getText().toString().trim();
                noHp = inNoHpRombongan.getText().toString();
                alamat = inAlamatRombongan.getText().toString().trim();

                if(namaFinal.isEmpty() ||
                        instisusiFinal.isEmpty() ||
                        agendaFinal.isEmpty() ||
                        jumlahRombongan.isEmpty() ||
                        noHp.isEmpty() ||
                        alamat.isEmpty()){
                    Toast.makeText(SatpamInputKunjunganActivity.this, "Lengkapi form!", Toast.LENGTH_LONG).show();
                }else{
                    aksi = "setKunjungan";
                    new ConnectionHelper().execute();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_no_animation, R.anim.push_out_right);
    }

    void setupActionBar(){
        getSupportActionBar().setElevation(3);
        getSupportActionBar().setTitle(title.toUpperCase());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

    }

    private class ConnectionHelper extends AsyncTask<String, Void, String> {
        ProgressDialog p = new ProgressDialog(SatpamInputKunjunganActivity.this);
        @Override
        protected void onPreExecute() {
            response = "";
            p.setTitle("Tunggu sebentar...");
            p.show();
        }

        @Override
        protected String doInBackground(String... params) {
            URL url = null;
            try {
                url = new URL(getResources().getString(R.string.ipaddr)+"act=setKunjungan");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                List<Pair> paramss = new ArrayList<Pair>();
                paramss.add(new Pair("tglKunjungan", tglFinal));
                paramss.add(new Pair("jamKunjungan", jamFinal));
                paramss.add(new Pair("nama", namaFinal));
                paramss.add(new Pair("institusi", instisusiFinal));
                paramss.add(new Pair("agenda", agendaFinal));
                paramss.add(new Pair("idPresensi", username));
                paramss.add(new Pair("jumlah_rombongan", jumlahRombongan));
                paramss.add(new Pair("noHp", noHp));
                paramss.add(new Pair("alamat", alamat));
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getQuery(paramss));
                writer.flush();
                writer.close();
                os.close();

                conn.connect();
                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line=br.readLine()) != null) {
                        response+=line;
                    }
                } else {
                    response = "";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            p.cancel();
            if (result != "") {
                Log.d("Ar-Rafi Log", result.replace("null", ""));
                try {
                    JSONObject json = new JSONObject(result.replace("null", ""));
                    if (json.getString("success").equals("1")) {
                        Toast.makeText(SatpamInputKunjunganActivity.this, "Input berhasil", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(SatpamInputKunjunganActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(SatpamInputKunjunganActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(SatpamInputKunjunganActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String getQuery(List<Pair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (Pair<String, String> pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.first, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.second, "UTF-8"));
        }

        return result.toString();
    }
}
