package com.bungkhus.ministudio.asih.view.tatausaha.activity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.adapter.tatausaha.KatuKunjunganAdapter;
import com.bungkhus.ministudio.asih.helper.DateHelper;
import com.bungkhus.ministudio.asih.helper.MenambahMengurangTanggal;
import com.bungkhus.ministudio.asih.helper.SessionManager;
import com.bungkhus.ministudio.asih.model.Kunjungan;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class KatuKunjunganActivity extends AppCompatActivity {

    private SessionManager session;
    private CatLoadingView loading;
    private HashMap<String, String> user;
    private String username, status, nama, title;
    private RecyclerView recyclerView;
    private List<Kunjungan> data = new ArrayList<>();
    private KatuKunjunganAdapter adapter;
    String url;
    final Calendar calendar = Calendar.getInstance();
    private TextView tvTanggal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_katu_kunjungan);
        session = new SessionManager(getApplicationContext());
        loading = new CatLoadingView();
        user = session.getData();
        username = user.get(SessionManager.KEY_USERNAME);
        status = user.get(SessionManager.KEY_STATUS);
        nama = user.get(SessionManager.KEY_NAMA);
        title = getIntent().getStringExtra("title");
        tvTanggal = (TextView) findViewById(R.id.tvTanggal);
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, dd MMM yyyy");
        tvTanggal.setText(dateFormat.format(calendar.getTime()));
        setupActionBar();
        url = getResources().getString(R.string.ipaddr)+"act=getAllKunjunganByDate&tgl=";
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(KatuKunjunganActivity.this));
        setRecyclerView(DateHelper.dateToString(DateHelper.stringToDate(tvTanggal.getText().toString(), new SimpleDateFormat("EEEE, dd MMM yyyy")), new SimpleDateFormat("yyyy-MM-dd")));
    }

    public void setRecyclerView(String tgl) {
        String final_url = url+tgl;
        data = getAllItemList(final_url);
        adapter = new KatuKunjunganAdapter(KatuKunjunganActivity.this, data, new KatuKunjunganAdapter.OnItemClickListener() {
            @Override
            public void onClick(Kunjungan item) {
                String title = item.getNama();
                String msg = "Alamat: "+item.getAlamat()
                        +"\n\nNo. HP: "+item.getNoHp()
                        +"\n\nJam Keluar: "+item.getJam_keluar();
                    viewMore(title, msg, String.valueOf(item.getId_kunjungan()));
            }
        });
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }

    void viewMore(String title, String msg, final String id_kunjungan){
        new AlertDialog.Builder(KatuKunjunganActivity.this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton("KEMBALI", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //
                    }
                })
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.rekap, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_rekap:
                getRekap(getResources().getString(R.string.ipaddr)+"act=getRekapKunjunganPerBulan&thn="+DateHelper.dateToString(DateHelper.stringToDate(tvTanggal.getText().toString(), new SimpleDateFormat("EEEE, dd MMM yyyy")), new SimpleDateFormat("yyyy"))+"&bln="+DateHelper.dateToString(DateHelper.stringToDate(tvTanggal.getText().toString(), new SimpleDateFormat("EEEE, dd MMM yyyy")), new SimpleDateFormat("M")));
                break;
            case R.id.action_date:
                showDatePicker();
                break;
        }
        return true;
    }

    private void showDatePicker(){
        DatePickerDialog d = new DatePickerDialog(KatuKunjunganActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar c2 = Calendar.getInstance();
                c2.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, dd MMM yyyy");
                tvTanggal.setText(dateFormat.format(c2.getTime()));
                setRecyclerView(DateHelper.dateToString(DateHelper.stringToDate(tvTanggal.getText().toString(), new SimpleDateFormat("EEEE, dd MMM yyyy")), new SimpleDateFormat("yyyy-MM-dd")));
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        d.show();
    }

    private void getRekap(final String url){
        loading.show(getSupportFragmentManager(),"");

        RequestQueue queue = Volley.newRequestQueue(KatuKunjunganActivity.this);
        StringRequest strReq = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String >(){
                    @Override
                    public void onResponse(String response) {
                        loading.dismiss();
                        Log.d("LOG","Response: "+response.toString());
                        try {
                            JSONArray array_sucess = new JSONArray(response);
                            int size = array_sucess.length();
                            String msg = "";
                            for(int i = 0; i<size; i++){
                                JSONObject data = array_sucess.getJSONObject(i);
                                Date bulan = DateHelper.stringToDate(data.getString("bulan"), new SimpleDateFormat("M"));
                                String tahun = data.getString("tahun");
                                String total = data.getString("total");
                                String institusi = data.getString("institusi");
                                String kali = data.getString("kali");
                                msg = msg+DateHelper.dateToString(bulan, new SimpleDateFormat("MMMM"))+" "
                                        +tahun
                                        +"\nTotal Kunjungan: "+total
                                        +"\nInstitusi Paling Sering: "+institusi+" ("+kali+").";
                            }
                            if(size == 0){
                                msg = "Tidak ada kunjungan.";
                            }
                            showRekapitulasi(msg);
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                Log.e("ERROR", "Error: " + error.getMessage());
                Toast.makeText(KatuKunjunganActivity.this,
                        "Gagal Menampilkan Data.", Toast.LENGTH_LONG).show();
            }
        });

        // Adding request to request queue
        queue.add(strReq);
    }

    void showRekapitulasi(String msg){
        new AlertDialog.Builder(KatuKunjunganActivity.this)
                .setTitle("Rekapitulasi Kunjungan")
                .setMessage(msg)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //
                    }
                })
                .show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_no_animation, R.anim.push_out_right);
    }

    void setupActionBar(){
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setTitle(title.toUpperCase());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

    }

    private List<Kunjungan> getAllItemList(final String url){

        final List<Kunjungan> allItems = new ArrayList<>();
//        loading.show(getSupportFragmentManager(), "");

        RequestQueue queue = Volley.newRequestQueue(KatuKunjunganActivity.this);
        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                loading.dismiss();
                Log.d("LOG", "Response: " + response.toString());

                try {
                    JSONArray array_success = new JSONArray(response);
                    int size = array_success.length();
                    allItems.clear();
                    Kunjungan model;
                    for (int i = 0; i<size; i++){
                        JSONObject data = array_success.getJSONObject(i);
                        int id_kunjungan = Integer.parseInt(data.getString("id_kunjungan"));
                        Date tgl_kunjungan = DateHelper.stringToDate(data.getString("tgl_kunjungan"), new SimpleDateFormat("yyyy-MM-dd"));
                        String jam_kunjungan = data.getString("jam_kunjungan");
                        String nama = data.getString("nama");
                        String institusi = data.getString("institusi");
                        String agenda = data.getString("agenda");
                        int jumlah_rombongan = Integer.parseInt(data.getString("jumlah_rombongan"));
                        String username = data.getString("username");
                        model = new Kunjungan(id_kunjungan, tgl_kunjungan, jam_kunjungan, nama, institusi, agenda, jumlah_rombongan, username);
                        allItems.add(model);
                        adapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                loading.dismiss();
                Log.e("ERROR", "Error: " + error.getMessage());
                Toast.makeText(KatuKunjunganActivity.this,
                        "Gagal Menampilkan Data.", Toast.LENGTH_LONG).show();
            }
        });

        // Adding request to request queue
        queue.add(strReq);

        return allItems;
    }
}
