package com.bungkhus.ministudio.asih.model;

import java.util.Date;

/**
 * Created by ahlul on 07/08/2016.
 */
public class PengajuanAlat {
    private String username;
    private String iddetail;
    private Date tgl_pengajuan;
    private String nama_barang;
    private String merk_barang;
    private int jumlah_barang;

    public PengajuanAlat() {
    }

    public PengajuanAlat(String username, Date tgl_pengajuan, String nama_barang, String merk_barang, int jumlah_barang) {
        this.username = username;
        this.tgl_pengajuan = tgl_pengajuan;
        this.nama_barang = nama_barang;
        this.merk_barang = merk_barang;
        this.jumlah_barang = jumlah_barang;
    }

    public String getIddetail() {
        return iddetail;
    }

    public void setIddetail(String iddetail) {
        this.iddetail = iddetail;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getTgl_pengajuan() {
        return tgl_pengajuan;
    }

    public void setTgl_pengajuan(Date tgl_pengajuan) {
        this.tgl_pengajuan = tgl_pengajuan;
    }

    public String getNama_barang() {
        return nama_barang;
    }

    public void setNama_barang(String nama_barang) {
        this.nama_barang = nama_barang;
    }

    public String getMerk_barang() {
        return merk_barang;
    }

    public void setMerk_barang(String merk_barang) {
        this.merk_barang = merk_barang;
    }

    public int getJumlah_barang() {
        return jumlah_barang;
    }

    public void setJumlah_barang(int jumlah_barang) {
        this.jumlah_barang = jumlah_barang;
    }
}

