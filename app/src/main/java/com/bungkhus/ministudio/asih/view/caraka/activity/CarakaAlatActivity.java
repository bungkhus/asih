package com.bungkhus.ministudio.asih.view.caraka.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.adapter.caraka.CarakaJadwalAdapter;
import com.bungkhus.ministudio.asih.adapter.caraka.CarakaPengajuanAlatAdapter;
import com.bungkhus.ministudio.asih.helper.DateHelper;
import com.bungkhus.ministudio.asih.helper.SessionManager;
import com.bungkhus.ministudio.asih.model.PengajuanAlat;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class CarakaAlatActivity extends AppCompatActivity {

    private CarakaPengajuanAlatAdapter adapter;
    private SessionManager session;
    private CatLoadingView loading;
    private RecyclerView recyclerView;
    private HashMap<String, String> user;
    private List<PengajuanAlat> data = new ArrayList<>();
    private String username, status, nama, title;
    String url;
    private Spinner spAlat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caraka_alat);
        session = new SessionManager(getApplicationContext());
        loading = new CatLoadingView();
        user = session.getData();
        username = user.get(SessionManager.KEY_USERNAME);
        status = user.get(SessionManager.KEY_STATUS);
        nama = user.get(SessionManager.KEY_NAMA);
        title = getIntent().getStringExtra("title");
        setupActionBar();
        url = getResources().getString(R.string.ipaddr)+"act=getAllAlatByUsernameAndProgress&username="+username+"&progress=";
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(CarakaAlatActivity.this));
//        setRecyclerView();
        spAlat = (Spinner) findViewById(R.id.spAlat);
        spAlat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String progress = spAlat.getSelectedItem().toString();
                if(progress.equals("Tidak Tersedia")){
                    progress = "Tidak%20Tersedia";
                }
                setRecyclerView(progress);
                System.out.println("iki - "+url+progress);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    public void setRecyclerView(String status){
        data = getAllItemList(url+status);
        adapter = new CarakaPengajuanAlatAdapter(CarakaAlatActivity.this, data, new CarakaPengajuanAlatAdapter.OnItemClickListener() {
            @Override
            public void onClick(PengajuanAlat item) {

            }
        }, this.status);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }

    private List<PengajuanAlat> getAllItemList(final String url){
        final List<PengajuanAlat> allItems = new ArrayList<>();
        loading.show(getSupportFragmentManager(),"");

        RequestQueue queue = Volley.newRequestQueue(CarakaAlatActivity.this);
        StringRequest strReq = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String >(){
                    @Override
                    public void onResponse(String response) {
                        loading.dismiss();
                        Log.d("LOG","Response: "+response.toString());
                        try {
                            JSONArray array_sucess = new JSONArray(response);
                            int size = array_sucess.length();
                            allItems.clear();
                            PengajuanAlat model;
                            for(int i = 0; i<size; i++){
                                JSONObject data = array_sucess.getJSONObject(i);
                                String username = data.getString("username");
                                Date tgl_pengajuan = DateHelper.stringToDate(data.getString("tgl_pengajuan"), new SimpleDateFormat("yyyy-MM-dd"));
                                String nama_barang = data.getString("nama_barang");
                                String merk_barang= data.getString("merk_barang");
                                int jumlah_barang = Integer.parseInt(data.getString("jumlah_permintaan"));
                                model = new PengajuanAlat(username,tgl_pengajuan,nama_barang,merk_barang,jumlah_barang);
                                model.setTgl_pengajuan(tgl_pengajuan);
                                allItems.add(model);
                                adapter.notifyDataSetChanged();
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                Log.e("ERROR", "Error: " + error.getMessage());
                Toast.makeText(CarakaAlatActivity.this,
                        "Gagal Menampilkan Data.", Toast.LENGTH_LONG).show();
            }
        });

        // Adding request to request queue
        queue.add(strReq);

        return allItems;
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.add, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_add:
                goTo(CarakaPengajuanAlatActivity.class, "PENGAJUAN ALAT");
                break;
        }
        return true;
    }

    void goTo(Class<?> cls, String title){
        Intent i = new Intent(CarakaAlatActivity.this, cls);
        i.putExtra("title", title);
        startActivity(i);
        overridePendingTransition(R.anim.pull_in_right, R.anim.activity_no_animation);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_no_animation, R.anim.push_out_right);
    }

    void setupActionBar(){
        getSupportActionBar().setElevation(3);
        getSupportActionBar().setTitle(title.toUpperCase());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

    }
}
