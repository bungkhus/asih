package com.bungkhus.ministudio.asih.view.caraka.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.adapter.MenuAdapter;
import com.bungkhus.ministudio.asih.adapter.caraka.CarakaJadwalAdapter;
import com.bungkhus.ministudio.asih.helper.DateHelper;
import com.bungkhus.ministudio.asih.helper.SessionManager;
import com.bungkhus.ministudio.asih.model.Jadwal;
import com.bungkhus.ministudio.asih.view.tatausaha.activity.KatuPresensiPegawaiActivity;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class CarakaJadwalActivity extends AppCompatActivity {


    private CarakaJadwalAdapter adapter;
    private SessionManager session;
    private CatLoadingView loading;
    private RecyclerView recyclerView;
    private HashMap<String, String> user;
    private List<Jadwal> data = new ArrayList<>();
    private String username, status, nama, title, id_pegawai;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caraka_jadwal);
        session = new SessionManager(getApplicationContext());
        loading = new CatLoadingView();
        user = session.getData();
        username = user.get(SessionManager.KEY_USERNAME);
        status = user.get(SessionManager.KEY_STATUS);
        nama = user.get(SessionManager.KEY_NAMA);
        id_pegawai = user.get(SessionManager.KEY_ID_PEGAWAI);
        title = getIntent().getStringExtra("title");
        setupActionBar();
        url = getResources().getString(R.string.ipaddr)+"act=getAllJadwalByUsername&username="+username;
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(CarakaJadwalActivity.this));
        setRecyclerView();
    }
     public void setRecyclerView(){
         data = getAllItemList(url);
         adapter = new CarakaJadwalAdapter(CarakaJadwalActivity.this, data, new CarakaJadwalAdapter.OnItemClickListener() {
             @Override
             public void onClick(Jadwal item) {
                 goToPresensi(KatuPresensiPegawaiActivity.class, item.getNama().toUpperCase(), item.getUsername(), item.getBol(), String.valueOf(item.getId_jadwal()), DateHelper.dateToString(item.getTgl_mulai(), new SimpleDateFormat("dd MMM yyyy")), DateHelper.dateToString(item.getTgl_selesai(), new SimpleDateFormat("dd MMM yyyy")), item.getTugas());
             }
         }, "caraka");
         adapter.notifyDataSetChanged();
         recyclerView.setAdapter(adapter);
     }

    void goToPresensi(Class<?> cls, String title, String un_ybs, String bol, String id, String tgl_mulai, String tgl_selesai, String tugas){
        Intent i = new Intent(CarakaJadwalActivity.this, cls);
        i.putExtra("title", title);
        i.putExtra("sebagai", "real_caraka");
        i.putExtra("un_ybs", un_ybs);
        i.putExtra("bol", bol);
        i.putExtra("id_jadwal", id);
        i.putExtra("tgl_mulai", tgl_mulai);
        i.putExtra("tgl_selesai", tgl_selesai);
        i.putExtra("tugas", tugas);
        i.putExtra("id_pegawai", id_pegawai);
        startActivity(i);
        overridePendingTransition(R.anim.pull_in_right, R.anim.activity_no_animation);
    }

    private List<Jadwal> getAllItemList(final String url){
        final List<Jadwal> allItems = new ArrayList<>();
        loading.show(getSupportFragmentManager(),"");

        RequestQueue queue = Volley.newRequestQueue(CarakaJadwalActivity.this);
        StringRequest strReq = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String >(){
                    @Override
                    public void onResponse(String response) {
                        loading.dismiss();
                        Log.d("LOG","Response: "+response.toString());
                        try {
                            JSONArray array_sucess = new JSONArray(response);
                            int size = array_sucess.length();
                            allItems.clear();
                            Jadwal model;
                            for(int i = 0; i<size; i++){
                                JSONObject data = array_sucess.getJSONObject(i);
                                int id_jadwal = Integer.parseInt(data.getString("id_jadwal"));
                                Date tgl_mulai = DateHelper.stringToDate(data.getString("tgl_mulai"), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
                                Date tgl_selesai = DateHelper.stringToDate(data.getString("tgl_selesai"), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
                                String tugas = data.getString("tugas");
                                String nama= data.getString("nama");
                                String username = data.getString("username");
                                String bol = data.getString("bol");
                                model = new Jadwal(id_jadwal,tgl_mulai,tgl_selesai,tugas,username,nama, bol);
                                allItems.add(model);
                                adapter.notifyDataSetChanged();
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                Log.e("ERROR", "Error: " + error.getMessage());
                Toast.makeText(CarakaJadwalActivity.this,
                        "Gagal Menampilkan Data.", Toast.LENGTH_LONG).show();
            }
        });

        // Adding request to request queue
        queue.add(strReq);

        return allItems;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_no_animation, R.anim.push_out_right);
    }

    void setupActionBar(){
        getSupportActionBar().setElevation(3);
        getSupportActionBar().setTitle(title.toUpperCase());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

    }
}
