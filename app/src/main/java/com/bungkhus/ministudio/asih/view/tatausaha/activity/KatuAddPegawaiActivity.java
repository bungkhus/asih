package com.bungkhus.ministudio.asih.view.tatausaha.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.helper.DateHelper;
import com.bungkhus.ministudio.asih.helper.SessionManager;
import com.bungkhus.ministudio.asih.model.Peristiwa;
import com.roger.catloadinglibrary.CatLoadingView;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

public class KatuAddPegawaiActivity extends AppCompatActivity {

    private HashMap<String, String> user;
    private String username, status, nama, title, sebagai, url, url_cek;
    private SessionManager session;
    private CatLoadingView loading;
    private TextView tvSebagai;
    private EditText etId, etNama, etTglLahir, etAlamat, etNoHp, etPhoto, etPassword;
    private Button btPilih, btSimpan;
    private ImageView pp;
    final Calendar c_mulai = Calendar.getInstance();
    //Image request code
    private int PICK_IMAGE_REQUEST = 1;

    //storage permission code
    private static final int STORAGE_PERMISSION_CODE = 123;

    //Bitmap to get image from gallery
    private Bitmap bitmap;

    //Uri to store the image uri
    private Uri filePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_katu_add_pegawai);
        inisialisasiKomponen();
        setupActionBar();
        setAwalId();
        actionCek();
        buttonAction();
    }

    private void inisialisasiKomponen(){
        session = new SessionManager(getApplicationContext());
        loading = new CatLoadingView();
        tvSebagai = (TextView) findViewById(R.id.tvSebagai);
        etId = (EditText) findViewById(R.id.etId);
        etNama = (EditText) findViewById(R.id.etNama);
        etTglLahir = (EditText) findViewById(R.id.etTglLahir);
        etAlamat = (EditText) findViewById(R.id.etAlamat);
        etNoHp = (EditText) findViewById(R.id.etNoHp);
        etPhoto = (EditText) findViewById(R.id.etPhoto);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btPilih = (Button) findViewById(R.id.btPilih);
        btSimpan = (Button) findViewById(R.id.btSimpan);
        pp = (ImageView) findViewById(R.id.pp);
        user = session.getData();
        username = user.get(SessionManager.KEY_USERNAME);
        status = user.get(SessionManager.KEY_STATUS);
        nama = user.get(SessionManager.KEY_NAMA);
        title = getIntent().getStringExtra("title");
        sebagai = getIntent().getStringExtra("sebagai");
        url = getResources().getString(R.string.global_ipaddr)+"upload.php";
        url_cek = getResources().getString(R.string.ipaddr)+"act=isPegawaiExist&id_pegawai=";
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_no_animation, R.anim.push_out_right);
    }

    private void setupActionBar(){
        getSupportActionBar().setElevation(3);
        getSupportActionBar().setTitle(title.toUpperCase());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

    }

    private void setAwalId(){
        if(sebagai.equalsIgnoreCase("caraka")){
            tvSebagai.setText("cr");
        }else{
            tvSebagai.setText("st");
        }
    }

    private boolean isInputNotNull(){
        return etAlamat.getText().length() > 0 &&
                etId.getText().length() > 0 &&
                etNama.getText().length() > 0 &&
                etNoHp.getText().length() > 0 &&
                etPhoto.getText().length() > 0 &&
                etTglLahir.getText().length() > 0;
    }

    private void cekExistingId(String id){
        loading.show(getSupportFragmentManager(), "");

        RequestQueue queue = Volley.newRequestQueue(KatuAddPegawaiActivity.this);
        StringRequest strReq = new StringRequest(Request.Method.GET,
                url_cek+""+id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                loading.dismiss();
                Log.d("LOG", "Response: " + response.toString());

                try {
                    JSONObject json = new JSONObject(response);
                    boolean isPegawaiExist = json.getBoolean("isPegawaiExist");
                    if(isPegawaiExist){
                        etId.setText("");
                        Toast.makeText(KatuAddPegawaiActivity.this, "Username atau Id sudah ada sebelumnya. Silahkan ganti dengan yang lain!", Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(KatuAddPegawaiActivity.this, "Username atau Id Tersedia.", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                Log.e("ERROR", "Error: " + error.getMessage());
                Toast.makeText(KatuAddPegawaiActivity.this,
                        "Gagal cek username.", Toast.LENGTH_LONG).show();
            }
        });

        // Adding request to request queue
        queue.add(strReq);
    }

    private void actionCek(){
        etNama.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!b){
                    System.out.println("focus");
                }else {
                    cekExistingId(tvSebagai.getText().toString()+""+etId.getText().toString());
                }
            }
        });
    }

    private void buttonAction(){
        btPilih.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChooser();
            }
        });

        btSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isInputNotNull()){
                    uploadMultipart(
                            tvSebagai.getText().toString()+""+etId.getText().toString(),
                            etNama.getText().toString(),
                            etTglLahir.getText().toString(),
                            etAlamat.getText().toString(),
                            "0"+etNoHp.getText().toString(),
                            etPassword.getText().toString());
                }else{
                    Toast.makeText(KatuAddPegawaiActivity.this, "Lengkapi Data!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        etTglLahir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Show DatePicker");
                showDatePicker();
            }
        });
    }

    private void showDatePicker(){
        DatePickerDialog d = new DatePickerDialog(KatuAddPegawaiActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar c2 = Calendar.getInstance();
                c2.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                etTglLahir.setText(dateFormat.format(c2.getTime()));
            }
        }, c_mulai.get(Calendar.YEAR), c_mulai.get(Calendar.MONTH), c_mulai.get(Calendar.DAY_OF_MONTH));
        d.show();
    }

    /*
    * This is the method responsible for image upload
    * We need the full image path and the name for the image in this method
    * */
    public void uploadMultipart(String id_pegawai, String nama, String tgl_lahir, String no_hp, String alamat, String password) {
        //getting name for the image
        String name = etPhoto.getText().toString().trim();

        //getting the actual path of the image
        String path = getPath(filePath);

        //Uploading code
        try {
            String uploadId = UUID.randomUUID().toString();

            //Creating a multi part request
            new MultipartUploadRequest(this, uploadId, url)
                    .addFileToUpload(path, "image")
                    .addParameter("id_pegawai", id_pegawai)
                    .addParameter("nama", nama)
                    .addParameter("tgl_lahir",tgl_lahir )
                    .addParameter("no_hp", no_hp)
                    .addParameter("alamat", alamat)
                    .addParameter("password", password)
                    .addParameter("bagian", sebagai)
                    .addParameter("foto", id_pegawai)
                    .addParameter("username", id_pegawai)
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(2)
                    .startUpload(); //Starting the upload

            Toast.makeText(this, "Input pegawai baru berhasil", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();

        } catch (Exception exc) {
            Toast.makeText(this, "error while uploading!", Toast.LENGTH_SHORT).show();
            exc.printStackTrace();
        }
    }

    //method to show file chooser
    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    //handling the image chooser activity result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                pp.setImageBitmap(bitmap);
                etPhoto.setText(getPath(filePath));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //method to get the file path from uri
    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }


    //Requesting permission
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }


    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }
}
