package com.bungkhus.ministudio.asih.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.helper.SessionManager;
import com.bungkhus.ministudio.asih.view.caraka.activity.CarakaMainActivity;
import com.bungkhus.ministudio.asih.view.satpam.activity.SatpamMainActivity;
import com.bungkhus.ministudio.asih.view.tatausaha.activity.KatuMainActivity;

import java.util.HashMap;

public class SplashScreenActivity extends AppCompatActivity {

    private SessionManager session;
    private int runtime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        session = new SessionManager(SplashScreenActivity.this);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                if (session.isLoggedIn()) {
                    runtime = 500;
                    HashMap<String, String> user = session.getData();
                    final String akses = user.get(SessionManager.KEY_STATUS);

                    if(akses.equalsIgnoreCase("KaTu")){
                        Intent intent = new Intent(SplashScreenActivity.this, KatuMainActivity.class);
                        startActivity(intent);
                        finish();
                    }else if(akses.equalsIgnoreCase("satpam")){
                        Intent intent = new Intent(SplashScreenActivity.this, SatpamMainActivity.class);
                        startActivity(intent);
                        finish();
                    }else if(akses.equalsIgnoreCase("caraka")){
                        Intent intent = new Intent(SplashScreenActivity.this, CarakaMainActivity.class);
                        startActivity(intent);
                        finish();
                    }else{
                        Toast.makeText(SplashScreenActivity.this, "Anda tidak memiliki akses ke aplikasi!", Toast.LENGTH_LONG).show();
                    }
                }else{
                    runtime = 1000;
                    Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }, runtime);
    }
}
