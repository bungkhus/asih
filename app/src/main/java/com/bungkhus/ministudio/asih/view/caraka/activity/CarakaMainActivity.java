package com.bungkhus.ministudio.asih.view.caraka.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.adapter.MenuAdapter;
import com.bungkhus.ministudio.asih.helper.SessionManager;
import com.bungkhus.ministudio.asih.model.Menu;
import com.bungkhus.ministudio.asih.view.AboutActivity;
import com.bungkhus.ministudio.asih.view.LoginActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CarakaMainActivity extends AppCompatActivity {

    private GridLayoutManager lLayout;
    private MenuAdapter adapter;
    private HashMap<String, String> user;
    private String nama;
    private SessionManager session;
    private List<Menu> menus = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caraka_main);
        session = new SessionManager(CarakaMainActivity.this);
        user = session.getData();
        setupActionBar();
        setupMenu();
        lLayout = new GridLayoutManager(CarakaMainActivity.this, 2);

        RecyclerView rView = (RecyclerView)findViewById(R.id.recycler_view);
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);

        adapter = new MenuAdapter(CarakaMainActivity.this, menus, new MenuAdapter.OnItemClickListener() {
            @Override
            public void onClick(Menu item) {
                String title = item.getNama();
                switch (item.getId()){
                    case 1:
                        goTo(CarakaJadwalActivity.class, title);
                        break;
                    case 2:
                        goTo(CarakaPresensiActivity.class, title);
                        break;
                    case 3:
                        goTo(CarakaAlatActivity.class, title);
                        break;
                    case 4:
                        goTo(CarakaKegiatanKebersihanActivity.class, title);
                        break;
                    case 5:
                        goTo(CarakaPenilaianActivity.class, title);
                        break;
                    case 6:
                        goTo(AboutActivity.class, title);
                        break;
                }
            }
        });
        rView.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    void setupMenu(){
        menus.add(new Menu(1, "Jadwal", ContextCompat.getDrawable(CarakaMainActivity.this, R.drawable.calendar)));
//        menus.add(new Menu(2, "Presensi", ContextCompat.getDrawable(CarakaMainActivity.this, R.drawable.medical_history)));
        menus.add(new Menu(3, "Pengajuan Alat", ContextCompat.getDrawable(CarakaMainActivity.this, R.drawable.broom)));
//        menus.add(new Menu(4, "Kegiatan Kebersihan", ContextCompat.getDrawable(CarakaMainActivity.this, R.drawable.washing)));
        menus.add(new Menu(5, "Penilaian", ContextCompat.getDrawable(CarakaMainActivity.this, R.drawable.list)));
        menus.add(new Menu(6, "Tentang", ContextCompat.getDrawable(CarakaMainActivity.this, R.drawable.info)));
    }

    void setupActionBar(){
        nama = user.get(SessionManager.KEY_NAMA);
        getSupportActionBar().setTitle("CARAKA - "+nama.toUpperCase());
        getSupportActionBar().setElevation(3);
    }

    void logout(){
        new AlertDialog.Builder(CarakaMainActivity.this)
                .setTitle("Keluar")
                .setMessage("Apakah anda yakin untuk keluar dari akun anda?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        session.setLogin(false);
                        Intent i = new Intent(CarakaMainActivity.this, LoginActivity.class);
                        startActivity(i);
                        finish();
                        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(R.drawable.alert)
                .show();
    }

    void about(){
        new AlertDialog.Builder(CarakaMainActivity.this)
                .setTitle("Tentang Aplikasi")
                .setMessage("ASIH \" Aman Dan Bersih \", adalah kebersihan dan pemantauan keamanan aplikasi berbasis pada os android di SD Ar-Rafi, Bandung.")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //
                    }
                })
                .show();
    }

    void goTo(Class<?> cls, String title){
        Intent i = new Intent(CarakaMainActivity.this, cls);
        i.putExtra("title", title);
        startActivity(i);
        overridePendingTransition(R.anim.pull_in_right, R.anim.activity_no_animation);
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.logout, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub

        switch (item.getItemId()) {

            case R.id.action_logout:
                logout();
                break;
        }
        return true;
    }

}
