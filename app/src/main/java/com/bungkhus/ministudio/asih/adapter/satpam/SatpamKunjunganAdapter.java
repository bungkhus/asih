package com.bungkhus.ministudio.asih.adapter.satpam;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.helper.DateHelper;
import com.bungkhus.ministudio.asih.model.Kunjungan;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by ahlul on 08/08/2016.
 */
public class SatpamKunjunganAdapter extends RecyclerView.Adapter<SatpamKunjunganAdapter.RecyclerViewHolders> {

    private final OnItemClickListener listener;
    private List<Kunjungan> itemList;
    private Context context;

    public SatpamKunjunganAdapter(Context context, List<Kunjungan> itemList, OnItemClickListener listener) {
        this.itemList = itemList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_satpam_kunjungan, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        holder.click(itemList.get(position), listener);
        holder.tvTanggal.setText("Tanggal :"+DateHelper.dateToString(itemList.get(position).getTgl_kunjungan(), new SimpleDateFormat("dd MMM yyyy")));
        holder.tvJam.setText("Jam : " + itemList.get(position).getJam_kunjungan());
        holder.tvNama.setText("Nama : "+itemList.get(position).getNama());
        //holder.tvJumlah.setText("Jml Rombongan : "+itemList.get(position).getJumlah_rombongan());
        holder.tvInstitusi.setText("Institusi : "+itemList.get(position).getInstitusi());
        holder.tvAgenda.setText("Agenda :"+itemList.get(position).getAgenda());

    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvTanggal, tvJam, tvNama, tvInstitusi, tvAgenda, tvJumlah;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvTanggal = (TextView) itemView.findViewById(R.id.tvTanggal);
            tvJam = (TextView) itemView.findViewById(R.id.tvJam);
            tvNama = (TextView) itemView.findViewById(R.id.tvNama);
            tvJumlah=(TextView) itemView.findViewById(R.id.tvJumlah);
            tvInstitusi =(TextView) itemView.findViewById(R.id.tvInstitusi);
            tvAgenda =(TextView) itemView.findViewById(R.id.tvAgenda);
        }

        public void click(final Kunjungan item, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(item);
                }
            });
        }

        @Override
        public void onClick(View view) {

        }
    }

    public interface OnItemClickListener {
        void onClick(Kunjungan item);
    }
}

