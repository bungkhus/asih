package com.bungkhus.ministudio.asih.adapter.tatausaha;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.helper.DateHelper;
import com.bungkhus.ministudio.asih.model.Peristiwa;
import com.bungkhus.ministudio.asih.view.MyPlayerActivity;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

/**
 * Created by bungkhus on 8/7/16.
 */
public class KatuPeristiwaAdapter  extends RecyclerView.Adapter<KatuPeristiwaAdapter.RecyclerViewHolders> {

    private final OnItemClickListener listener;
    private List<Peristiwa> itemList;
    private Context context;
    private String sebagai;

    public KatuPeristiwaAdapter(Context context, List<Peristiwa> itemList, OnItemClickListener listener, String sebagai) {
        this.itemList = itemList;
        this.context = context;
        this.listener = listener;
        this.sebagai = sebagai;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_katu_peristiwa, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, final int position) {
        holder.click(itemList.get(position), listener);
        holder.tvTanggal.setText(DateHelper.dateToString(itemList.get(position).getTgl_peristiwa(), new SimpleDateFormat("dd MMM yyyy"))+", "+itemList.get(position).getJam_peristiwa()+"\nSaksi:");
        holder.tvKejadian.setText(itemList.get(position).getKejadian());
        holder.tvDesc.setText(itemList.get(position).getDeskripsi()+".");
        holder.tvDesc.append("\nStatus: "+itemList.get(position).getStatus_penyelesaian());
        int N = itemList.get(position).getSaksi().size();
        final TextView[] myTextViews = new TextView[N];
        for (int i = 0; i < N; i++) {
            final TextView rowTextView = new TextView(context);

            rowTextView.setText(itemList.get(position).getSaksi().get(i));
            rowTextView.setTextColor(context.getResources().getColor(R.color.white));
            rowTextView.setTextSize(13);
            rowTextView.setPadding(6,3,6,3);
            rowTextView.setBackgroundColor(context.getResources().getColor(R.color.secondary_text));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((ViewGroup.MarginLayoutParams)(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)));
            params.setMargins(0,0,3,0);
            rowTextView.setLayoutParams(params);

            holder.saksi_view.addView(rowTextView);

            myTextViews[i] = rowTextView;
        }
        Arrays.fill( myTextViews, null );
        N = 0;

        holder.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, MyPlayerActivity.class);
                i.putExtra("video", itemList.get(position).getVideo());
                context.startActivity(i);
            }
        });

        String foto = itemList.get(position).getFoto();
        String url = context.getResources().getString(R.string.ipaddr_media)+foto;

        if(!foto.equalsIgnoreCase("null") || foto != null || !foto.equalsIgnoreCase("")){
            holder.tvKejadian.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvTanggal.setTextColor(context.getResources().getColor(R.color.white));
            Glide
                    .with(context)
                    .load(url)
                    .centerCrop()
                    .placeholder(R.drawable.noimage)
                    .crossFade()
                    .into(holder.imgPeristiwa);
        }

        if(sebagai.equalsIgnoreCase("satpam") && itemList.get(position).getStatus_penyelesaian().equalsIgnoreCase("belum")){
            holder.tvSetSelesai.setVisibility(View.VISIBLE);
            holder.tvSetSelesai.setTextColor(context.getResources().getColor(R.color.white));
            holder.tvSetSelesai.setText("UBAH STATUS");
        }
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView imgPeristiwa;
        TextView tvKejadian;
        TextView tvTanggal;
        TextView tvDesc;
        TextView tvSetSelesai;
        LinearLayout saksi_view;
        FloatingActionButton fab;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            imgPeristiwa = (ImageView) itemView.findViewById(R.id.imgPeristiwa);
            tvKejadian = (TextView) itemView.findViewById(R.id.tvKejadian);
            tvTanggal = (TextView) itemView.findViewById(R.id.tvTanggal);
            tvDesc = (TextView) itemView.findViewById(R.id.tvDesc);
            tvSetSelesai = (TextView) itemView.findViewById(R.id.tvSetSelesai);
            saksi_view = (LinearLayout) itemView.findViewById(R.id.saksi_view);
            fab = (FloatingActionButton) itemView.findViewById(R.id.fab);
        }

        public void click(final Peristiwa item, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(item);
                }
            });
        }

        @Override
        public void onClick(View view) {

        }
    }

    public interface OnItemClickListener {
        void onClick(Peristiwa item);
    }
}
