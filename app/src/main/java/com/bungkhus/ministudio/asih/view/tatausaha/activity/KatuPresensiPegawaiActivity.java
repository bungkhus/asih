package com.bungkhus.ministudio.asih.view.tatausaha.activity;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.adapter.caraka.CarakaPresensiAdapter;
import com.bungkhus.ministudio.asih.adapter.tatausaha.KatuPeristiwaAdapter;
import com.bungkhus.ministudio.asih.helper.DateHelper;
import com.bungkhus.ministudio.asih.helper.SessionManager;
import com.bungkhus.ministudio.asih.model.KegiatanKebersihan;
import com.bungkhus.ministudio.asih.model.Peristiwa;
import com.bungkhus.ministudio.asih.model.Presensi;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KatuPresensiPegawaiActivity extends AppCompatActivity {

    private HashMap<String, String> user;
    private String username, status, nama, title, bol, un_ybs, id_jadwal, tanggal, tugas, id_pegawai, sebagai;
    private SessionManager session;
    private CatLoadingView loading;
    private RecyclerView recyclerView;
    private List<Presensi> data = new ArrayList<>();
    private CarakaPresensiAdapter adapter;
    private String url, url_set_absen;
    private TextView tvDetail;
    private Menu menu_item;
//    private boolean action_add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_katu_add_presensi_pegawai);
        session = new SessionManager(getApplicationContext());
        loading = new CatLoadingView();
        tvDetail = (TextView) findViewById(R.id.tvDetail);
        user = session.getData();
        username = user.get(SessionManager.KEY_USERNAME);
        status = user.get(SessionManager.KEY_STATUS);
        nama = user.get(SessionManager.KEY_NAMA);
        title = getIntent().getStringExtra("title");
        bol = getIntent().getStringExtra("bol");
        un_ybs = getIntent().getStringExtra("un_ybs");
        id_jadwal = getIntent().getStringExtra("id_jadwal");
        tanggal = getIntent().getStringExtra("tgl_mulai")+" s.d. "+getIntent().getStringExtra("tgl_selesai");
        tugas = getIntent().getStringExtra("tugas");
        sebagai = getIntent().getStringExtra("sebagai");
        id_pegawai = getIntent().getStringExtra("id_pegawai");
        setupActionBar();
        url = getResources().getString(R.string.ipaddr)+"act=getAllPresensiByIdJadwal&id_jadwal="+id_jadwal;
        url_set_absen = getResources().getString(R.string.ipaddr)+"act=setPresensi";
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(KatuPresensiPegawaiActivity.this));
        setRecyclerView();
    }

    public void setRecyclerView(){
        data = getAllItemList(url);
        System.out.println("anu.."+sebagai);
        adapter = new CarakaPresensiAdapter(KatuPresensiPegawaiActivity.this, data, new CarakaPresensiAdapter.OnItemClickListener() {
            @Override
            public void onClick(Presensi item) {
                if(sebagai.equalsIgnoreCase("satpam")){
                    System.out.println("do nothing");
                }else{
                    if(item.getStatus_presensi().equalsIgnoreCase("hadir")){
                        getAllKegiatanKebersihan(getResources().getString(R.string.ipaddr)+"act=getAllKebersihanByPresensi&id_presensi="+item.getId_presensi(), String.valueOf(item.getId_presensi()));
                    }
                }
            }
        }, sebagai);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
        isAbsenExist();
    }

    void showKegiatanKebersihan(String msg){
        new AlertDialog.Builder(KatuPresensiPegawaiActivity.this)
                .setTitle("Kegiatan Kebersihan")
                .setMessage(msg)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //
                    }
                })
                .show();
    }

    void showAdaAddKegiatanKebersihan(String msg, final String id_presensi){
        new AlertDialog.Builder(KatuPresensiPegawaiActivity.this)
                .setTitle("Kegiatan Kebersihan")
                .setMessage(msg)
                .setPositiveButton("TAMBAH", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        showTambahKegiatan(id_presensi);
                    }
                })
                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //
                    }
                })
                .show();
    }

    void showTambahKegiatan(final String id_presensi){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog_add_kegiatan_kebersihan, null);
        dialogBuilder.setView(dialogView);

        final Spinner spAlat = (Spinner) dialogView.findViewById(R.id.spAlat);
        // --- get kegiatan --- //
        loading.show(getSupportFragmentManager(),"");

        final List<String> id_keg = new ArrayList<>();
        final List<String> kegiatan = new ArrayList<>();

        RequestQueue queue = Volley.newRequestQueue(KatuPresensiPegawaiActivity.this);
        StringRequest strReq = new StringRequest(Request.Method.GET, getResources().getString(R.string.ipaddr)+"act=getAllKegiatanKebersihan",
                new Response.Listener<String >(){
                    @Override
                    public void onResponse(String response) {
                        loading.dismiss();
                        Log.d("LOG","Response: "+response.toString());
                        try {
                            JSONArray array_sucess = new JSONArray(response);
                            int size = array_sucess.length();
                            kegiatan.clear();
                            id_keg.clear();
                            for(int i = 0; i<size; i++){
                                JSONObject data = array_sucess.getJSONObject(i);
                                String id_nama = data.getString("id_nama");
                                String nama_kegiatan = data.getString("nama_kegiatan");
                                id_keg.add(id_nama);
                                kegiatan.add(nama_kegiatan);
                            }
                            spAlat.setAdapter(new ArrayAdapter<String>(KatuPresensiPegawaiActivity.this, android.R.layout.simple_spinner_dropdown_item, kegiatan));
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                Log.e("ERROR", "Error: " + error.getMessage());
                Toast.makeText(KatuPresensiPegawaiActivity.this,
                        "Gagal Menampilkan Data.", Toast.LENGTH_LONG).show();
            }
        });

        // Adding request to request queue
        queue.add(strReq);

        // --- end --- //

        final EditText etKeterangan = (EditText) dialogView.findViewById(R.id.etKeterangan);

        dialogBuilder.setTitle("CATAT KEGIATAN KEBERSIHAN");
        dialogBuilder.setPositiveButton("SIMPAN", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                System.out.println("Emm.. "+id_keg.get(spAlat.getSelectedItemPosition()));
                postDataKegiatan(id_presensi, id_keg.get(spAlat.getSelectedItemPosition()), DateHelper.dateToString(new Date(), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")), etKeterangan.getText().toString());
            }
        });
        dialogBuilder.setNegativeButton("Kembali", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    private void postDataKegiatan(final String id_presensi, final String id_nama, final String waktu, final String keterangan){
        loading.show(getSupportFragmentManager(), "");
        RequestQueue queue = Volley.newRequestQueue(KatuPresensiPegawaiActivity.this);
        StringRequest strReq = new StringRequest(Request.Method.POST,
                getResources().getString(R.string.ipaddr)+"act=setKegiatanKebersihan", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                loading.dismiss();

                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("success");

                    // Check for error node in json
                    if (status.equalsIgnoreCase("1")) {
                        Toast.makeText(KatuPresensiPegawaiActivity.this,
                                "Kegiatan Kebersihan berhasil terkirim!", Toast.LENGTH_LONG).show();
                    } else {
                        // Error in login. Get the error message
                        Toast.makeText(KatuPresensiPegawaiActivity.this,
                                "Gagal mengirim data!", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Toast.makeText(KatuPresensiPegawaiActivity.this,
                            "Gagal mengirim data!", Toast.LENGTH_LONG).show();
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                Log.e("ERROR", "Submit Error: " + error.getMessage());
                Toast.makeText(KatuPresensiPegawaiActivity.this,
                        "Submit Failed", Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("waktu", waktu);
                params.put("id_presensi", id_presensi);
                params.put("id_nama", id_nama);
                params.put("ket", keterangan);
                return params;
            }

        };

        // Adding request to request queue
        queue.add(strReq);
    }

    private void getAllKegiatanKebersihan(final String url, final String id_presensi){
        final List<KegiatanKebersihan> allItems = new ArrayList<>();
        loading.show(getSupportFragmentManager(),"");

        RequestQueue queue = Volley.newRequestQueue(KatuPresensiPegawaiActivity.this);
        StringRequest strReq = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String >(){
                    @Override
                    public void onResponse(String response) {
                        loading.dismiss();
                        Log.d("LOG","Response: "+response.toString());
                        try {
                            JSONArray array_sucess = new JSONArray(response);
                            int size = array_sucess.length();
                            allItems.clear();
                            KegiatanKebersihan model= new KegiatanKebersihan();
                            String msg = "";
                            for(int i = 0; i<size; i++){
                                JSONObject data = array_sucess.getJSONObject(i);
                                Date waktu = DateHelper.stringToDate(data.getString("waktu"), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
                                String nama_kegiatan = data.getString("nama_kegiatan");
                                String keterangan = data.getString("keterangan");
                                model.setNama_kegiatan(nama_kegiatan);
                                model.setWaktu(waktu);
                                model.setKeterangan(keterangan);
                                allItems.add(model);
                                msg = msg+DateHelper.dateToString(waktu, new SimpleDateFormat("HH:mm"))+"\n"+nama_kegiatan;
                                if (keterangan == null || keterangan.equalsIgnoreCase("null") || keterangan.equalsIgnoreCase("") || keterangan.length() < 1){
                                    msg = msg+"\n\n";
                                }else{
                                    msg = msg+" ("+keterangan+")\n\n";
                                }
                            }
                            if(size == 0){
                                msg = "Tidak ada kegiatan kebersihan.";
                            }

                            if(sebagai.equals("real_caraka")){
                                showAdaAddKegiatanKebersihan(msg, id_presensi);
                            }else{
                                showKegiatanKebersihan(msg);
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                Log.e("ERROR", "Error: " + error.getMessage());
                Toast.makeText(KatuPresensiPegawaiActivity.this,
                        "Gagal Menampilkan Data.", Toast.LENGTH_LONG).show();
            }
        });

        // Adding request to request queue
        queue.add(strReq);
    }

    public void setPresensi() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog_set_presensi, null);
        dialogBuilder.setView(dialogView);

        final Spinner edt = (Spinner) dialogView.findViewById(R.id.spAbsen);
        final EditText etAlasan = (EditText) dialogView.findViewById(R.id.etAlasan);
        edt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(edt.getSelectedItem().equals("Ijin")){
                    etAlasan.setVisibility(View.VISIBLE);
                }else{
                    etAlasan.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        dialogBuilder.setTitle("INPUT PRESENSI HARI INI");
        dialogBuilder.setPositiveButton("SIMPAN", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String alasan;
                if(edt.getSelectedItem().toString().equalsIgnoreCase("Ijin")){
                    alasan = etAlasan.getText().toString();
                }else{
                    alasan = "-";
                }
                if(alasan.length() == 0){
                    Toast.makeText(KatuPresensiPegawaiActivity.this,
                            "Kirim Absen Gagal. Isikan Alasan!", Toast.LENGTH_LONG).show();
                }else{
                    String tgl = DateHelper.dateToString(new Date(), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
                    String status = edt.getSelectedItem().toString();
                    postData(tgl, status, alasan);
                }
            }
        });
        dialogBuilder.setNegativeButton("Kembali", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    private void postData(final String tglAbsen, final String status, final String alasan){
        loading.show(getSupportFragmentManager(), "");
        RequestQueue queue = Volley.newRequestQueue(KatuPresensiPegawaiActivity.this);
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url_set_absen, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                loading.dismiss();

                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("success");

                    // Check for error node in json
                    if (status.equalsIgnoreCase("1")) {
                        Toast.makeText(KatuPresensiPegawaiActivity.this,
                                "Presensi berhasil terkirim!", Toast.LENGTH_LONG).show();
                        setRecyclerView();
                    } else {
                        // Error in login. Get the error message
                        Toast.makeText(KatuPresensiPegawaiActivity.this,
                                "Gagal mengirim data!", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Toast.makeText(KatuPresensiPegawaiActivity.this,
                            "Gagal mengirim data!", Toast.LENGTH_LONG).show();
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                Log.e("ERROR", "Submit Error: " + error.getMessage());
                Toast.makeText(KatuPresensiPegawaiActivity.this,
                        "Submit Failed", Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("tglAbsen", tglAbsen);
                params.put("status", status);
                params.put("alasan", alasan);
                params.put("id_pegawai", id_pegawai);
                params.put("id_jadwal", id_jadwal);
                return params;
            }

        };

        // Adding request to request queue
        queue.add(strReq);
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("ini resume");
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.add, menu);
        menu_item = menu;
        if(bol.equalsIgnoreCase("0")){
            return false;
        }else{
            return true;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_add:
                setPresensi();
//                System.out.println("nganu..."+action_add);
//                if (action_add){
//                    setPresensi();
//                }else{
//                    Toast.makeText(KatuPresensiPegawaiActivity.this, "Anda sudah absen hari ini!", Toast.LENGTH_LONG).show();
//                }
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_no_animation, R.anim.push_out_right);
    }

    void setupActionBar(){
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setTitle("PRESENSI "+title.toUpperCase());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        tvDetail.setText(tanggal+"\n"+tugas);
    }

    private List<Presensi> getAllItemList(final String url){
        final List<Presensi> allItems = new ArrayList<>();
        loading.show(getSupportFragmentManager(),"");

        RequestQueue queue = Volley.newRequestQueue(KatuPresensiPegawaiActivity.this);
        StringRequest strReq = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String >(){
                    @Override
                    public void onResponse(String response) {
                        loading.dismiss();
                        Log.d("LOG","Response: "+response.toString());
                        try {
                            JSONArray array_sucess = new JSONArray(response);
                            int size = array_sucess.length();
                            allItems.clear();
                            Presensi model;
                            for(int i = 0; i<size; i++){
                                JSONObject data = array_sucess.getJSONObject(i);
                                int id_presensi = Integer.parseInt(data.getString("id_presensi"));
                                Date tgl_absen = DateHelper.stringToDate(data.getString("tgl_absen"), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
                                Date pulang;
                                if(data.getString("pulang").equalsIgnoreCase("null") || data.getString("pulang") == null){
                                    pulang = tgl_absen;
                                }else{
                                    pulang = DateHelper.stringToDate(data.getString("pulang"), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
                                }
                                String status_presensi = data.getString("status_presensi");
                                String alasan = data.getString("alasan");
                                String id_pegawai = data.getString("id_pegawai");
                                int id_jadwal = Integer.parseInt(data.getString("id_jadwal"));
                                model = new Presensi(id_presensi,tgl_absen,status_presensi,alasan,id_pegawai,id_jadwal);
                                model.setTgl_absen(tgl_absen);
                                model.setPulang(pulang);
                                allItems.add(model);
                                adapter.notifyDataSetChanged();
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                Log.e("ERROR", "Error: " + error.getMessage());
                Toast.makeText(KatuPresensiPegawaiActivity.this,
                        "Gagal Menampilkan Data.", Toast.LENGTH_LONG).show();
            }
        });

        // Adding request to request queue
        queue.add(strReq);

        return allItems;
    }

    private void isAbsenExist(){
        RequestQueue queue = Volley.newRequestQueue(KatuPresensiPegawaiActivity.this);
        StringRequest strReq = new StringRequest(Request.Method.GET, getResources().getString(R.string.ipaddr)+"act=presensiTodayIsExist&id_jadwal="+id_jadwal,
                new Response.Listener<String >(){
                    @Override
                    public void onResponse(String response) {
                        Log.d("LOG","Response: "+response.toString());
                        try {
                            JSONArray array_sucess = new JSONArray(response);
                            int size = array_sucess.length();
                            for(int i = 0; i<size; i++){
                                JSONObject data = array_sucess.getJSONObject(i);
                                String exist = data.getString("exist");
                                System.out.println("emm.. "+exist);
                                if(exist.equalsIgnoreCase("1")){
//                                    menu_item.findItem(R.id.action_add).setVisible(false);
//                                    menu_item.findItem(R.id.action_add).setEnabled(false);
//                                    invalidateOptionsMenu();
                                    menu_item.removeItem(R.id.action_add);
                                    System.out.println("xxx");
                                }
//                                action_add = menu_item.findItem(R.id.action_add).isVisible();
//                                System.out.println("anu.. "+action_add);
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                Log.e("ERROR", "Error: " + error.getMessage());
                Toast.makeText(KatuPresensiPegawaiActivity.this,
                        "Terjadi Kesalahan.", Toast.LENGTH_LONG).show();
            }
        });

        // Adding request to request queue
        queue.add(strReq);

    }
}
