package com.bungkhus.ministudio.asih.view.caraka.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.helper.SessionManager;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class CarakaPengajuanAlatActivity extends AppCompatActivity {

    private SessionManager session;
    private CatLoadingView loading;
    private HashMap<String, String> user;
    private String username, status, nama, title;

    EditText inTglAlat, inJumlahBarang, inKetBarang;
    Spinner listBarangs;
    Button bInputAlat;
    ArrayList<String> listBarang = new ArrayList<String>();
    ArrayList<String> listKodeBarang = new ArrayList<String>();
    String tglFinal, namaBarang, barangFinal, jumlahFinal, ketFinal, idPegawaiFinal, response, aksi;
    ArrayAdapter<String> listBarangAdapter;
    SimpleDateFormat dateFormat, dateFormat2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caraka_pengajuan_alat);
        session = new SessionManager(getApplicationContext());
        loading = new CatLoadingView();
        user = session.getData();
        username = user.get(SessionManager.KEY_USERNAME);
        status = user.get(SessionManager.KEY_STATUS);
        nama = user.get(SessionManager.KEY_NAMA);
        title = getIntent().getStringExtra("title");
        setupActionBar();
        setupComponent();
    }

    private void setupComponent(){

        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat2 = new SimpleDateFormat("HH:mm");
        final Calendar c = Calendar.getInstance();
        tglFinal = dateFormat.format(c.getTime());

        inTglAlat = (EditText) findViewById(R.id.inTglAlat);
        inJumlahBarang = (EditText) findViewById(R.id.inJumlahBarang);
        inKetBarang = (EditText) findViewById(R.id.inKetBarang);
        listBarangs = (Spinner) findViewById(R.id.listBarang);
        bInputAlat = (Button) findViewById(R.id.bInputAlat);
        inTglAlat.setText(tglFinal);

        inTglAlat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog d = new DatePickerDialog(CarakaPengajuanAlatActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar c2 = Calendar.getInstance();
                        c2.set(year, monthOfYear, dayOfMonth);
                        tglFinal = dateFormat.format(c2.getTime());
                        inTglAlat.setText(tglFinal);
                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                d.show();
            }
        });
        bInputAlat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumlahFinal = inJumlahBarang.getText().toString();
                barangFinal = listKodeBarang.get(listBarangs.getSelectedItemPosition());
                namaBarang = listBarangs.getSelectedItem().toString();
                ketFinal = inKetBarang.getText().toString();

                aksi = "set_alat";
                new ConnectionHelper().execute();
            }
        });
        aksi = "get_barang";
        new ConnectionHelper().execute();
    }

    private class ConnectionHelper extends AsyncTask<String, Void, String> {
        ProgressDialog p = new ProgressDialog(CarakaPengajuanAlatActivity.this);
        @Override
        protected void onPreExecute() {
            response = "";
            p.setTitle("Tunggu sebentar...");
            p.show();
        }

        @Override
        protected String doInBackground(String... params) {
            URL url = null;
            try {
                List<Pair> paramss = new ArrayList<Pair>();
                if (aksi.equals("get_barang")) {
                    url = new URL(getResources().getString(R.string.ipaddr)+"act=getAllBarang");
                } else if (aksi.equals("get_idpegawai")) {
                    url = new URL(getResources().getString(R.string.ipaddr)+"act=getAllPegawaiByUsername");
                    paramss.add(new Pair("username", username));
                } else {
                    url = new URL(getResources().getString(R.string.ipaddr)+"act=setAlat");
                    paramss.add(new Pair("tgl", tglFinal));
                    paramss.add(new Pair("barang", barangFinal));
                    paramss.add(new Pair("jml", jumlahFinal));
                    paramss.add(new Pair("ket", ketFinal));
                    paramss.add(new Pair("id_pegawai", idPegawaiFinal));
                }
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getQuery(paramss));
                writer.flush();
                writer.close();
                os.close();

                conn.connect();
                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line=br.readLine()) != null) {
                        response+=line;
                    }
                } else {
                    response = "";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            p.cancel();
            if (result != "") {
                Log.d("Ar-Rafi Log", result.replace("null",""));
                try {
                    if (aksi.equals("get_barang")) {
                        JSONArray json = new JSONArray(result.replace("null",""));
                        for(int i = 0;i < json.length(); i++) {
                            listBarang.add(json.getJSONObject(i).getString("nama_barang"));
                            listKodeBarang.add(json.getJSONObject(i).getString("kode_barang"));
                        }
                        listBarangAdapter = new ArrayAdapter<String>(CarakaPengajuanAlatActivity.this, android.R.layout.simple_spinner_item, listBarang);
                        listBarangs.setAdapter(listBarangAdapter);

                        aksi = "get_idpegawai";
                        new ConnectionHelper().execute();
                    } else if (aksi.equals("get_idpegawai")) {
                        JSONArray json = new JSONArray(result.replace("null",""));
                        for(int i = 0;i < json.length(); i++) {
                            idPegawaiFinal = json.getJSONObject(i).getString("id_pegawai");
                        }
                    } else {
                        JSONObject json = new JSONObject(result.replace("null", ""));
                        if (json.getString("success").equals("1")) {

                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                    CarakaPengajuanAlatActivity.this);

                            // set title
                            alertDialogBuilder.setTitle("BERHASIL");

                            // set dialog message
                            alertDialogBuilder
                                    .setMessage("Pengajuan alat "+barangFinal+", sejumlah: "+jumlahFinal+" berhasil.\nAjukan alat lain?")
                                    .setCancelable(false)
                                    .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,int id) {
                                            dialog.cancel();
                                            inJumlahBarang.setText("");
                                            inKetBarang.setText("");
                                            listBarangs.setSelection(0);
                                        }
                                    })
                                    .setNegativeButton("No",new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            onBackPressed();                                       }
                                    });

                            // create alert dialog
                            AlertDialog alertDialog = alertDialogBuilder.create();

                            // show it
                            alertDialog.show();

                        } else {
                            Toast.makeText(CarakaPengajuanAlatActivity.this, "Error", Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(CarakaPengajuanAlatActivity.this, "Tidak ada data", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(CarakaPengajuanAlatActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String getQuery(List<Pair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (Pair<String, String> pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.first, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.second, "UTF-8"));
        }

        return result.toString();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_no_animation, R.anim.push_out_right);
    }

    void setupActionBar(){
        getSupportActionBar().setElevation(3);
        getSupportActionBar().setTitle(title.toUpperCase());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
    }
}
