package com.bungkhus.ministudio.asih.model;

import java.util.Date;

/**
 * Created by bungkhus on 8/7/16.
 */
public class Kunjungan {

    int id_kunjungan;
    Date tgl_kunjungan;
    String jam_kunjungan;
    String nama;
    String institusi;
    String agenda;
    int jumlah_rombongan;
    String username;
    String alamat;
    String noHp;
    String jam_keluar;

    public Kunjungan() {
    }

    public Kunjungan(int id_kunjungan, Date tgl_kunjungan, String jam_kunjungan, String nama, String institusi, String agenda, int jumlah_rombongan, String username) {
        this.id_kunjungan = id_kunjungan;
        this.tgl_kunjungan = tgl_kunjungan;
        this.jam_kunjungan = jam_kunjungan;
        this.nama = nama;
        this.institusi = institusi;
        this.agenda = agenda;
        this.jumlah_rombongan = jumlah_rombongan;
        this.username = username;
    }

    public String getJam_keluar() {
        return jam_keluar;
    }

    public void setJam_keluar(String jam_keluar) {
        this.jam_keluar = jam_keluar;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public int getId_kunjungan() {
        return id_kunjungan;
    }

    public void setId_kunjungan(int id_kunjungan) {
        this.id_kunjungan = id_kunjungan;
    }

    public Date getTgl_kunjungan() {
        return tgl_kunjungan;
    }

    public void setTgl_kunjungan(Date tgl_kunjungan) {
        this.tgl_kunjungan = tgl_kunjungan;
    }

    public String getJam_kunjungan() {
        return jam_kunjungan;
    }

    public void setJam_kunjungan(String jam_kunjungan) {
        this.jam_kunjungan = jam_kunjungan;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getInstitusi() {
        return institusi;
    }

    public void setInstitusi(String institusi) {
        this.institusi = institusi;
    }

    public String getAgenda() {
        return agenda;
    }

    public void setAgenda(String agenda) {
        this.agenda = agenda;
    }

    public int getJumlah_rombongan() {
        return jumlah_rombongan;
    }

    public void setJumlah_rombongan(int jumlah_rombongan) {
        this.jumlah_rombongan = jumlah_rombongan;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
