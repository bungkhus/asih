package com.bungkhus.ministudio.asih.model;

import java.util.Date;

/**
 * Created by ahlul on 07/08/2016.
 */
public class Jadwal {
    private int id_jadwal;
    private Date tgl_mulai;
    private Date tgl_selesai;
    private String tugas;
    private String username;
    private String nama;
    private String bol;

    public Jadwal() {
    }

    public Jadwal(int id_jadwal, Date tgl_mulai, Date tgl_selesai, String tugas, String username, String nama, String bol) {
        this.id_jadwal = id_jadwal;
        this.tgl_mulai = tgl_mulai;
        this.tgl_selesai = tgl_selesai;
        this.tugas = tugas;
        this.username = username;
        this.nama = nama;
        this.bol = bol;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getId_jadwal() {
        return id_jadwal;
    }

    public void setId_jadwal(int id_jadwal) {
        this.id_jadwal = id_jadwal;
    }

    public Date getTgl_mulai() {
        return tgl_mulai;
    }

    public void setTgl_mulai(Date tgl_mulai) {
        this.tgl_mulai = tgl_mulai;
    }

    public Date getTgl_selesai() {
        return tgl_selesai;
    }

    public void setTgl_selesai(Date tgl_selesai) {
        this.tgl_selesai = tgl_selesai;
    }

    public String getTugas() {
        return tugas;
    }

    public void setTugas(String tugas) {
        this.tugas = tugas;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBol() {
        return bol;
    }

    public void setBol(String bol) {
        this.bol = bol;
    }
}
