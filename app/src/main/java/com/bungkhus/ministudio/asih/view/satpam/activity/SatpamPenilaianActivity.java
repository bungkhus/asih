package com.bungkhus.ministudio.asih.view.satpam.activity;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.adapter.caraka.CarakaPenilaianAdapter;
import com.bungkhus.ministudio.asih.adapter.satpam.SatpamPenilaianAdapter;
import com.bungkhus.ministudio.asih.helper.SessionManager;
import com.bungkhus.ministudio.asih.model.Penilaian;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import lecho.lib.hellocharts.listener.LineChartOnValueSelectListener;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.ValueShape;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.LineChartView;

public class SatpamPenilaianActivity extends AppCompatActivity {

    private GridLayoutManager lLayout;
    private CarakaPenilaianAdapter adapter;
    private SessionManager session;
    private CatLoadingView loading;
    private RecyclerView recyclerView;
    private HashMap<String, String> user;
    private List<Penilaian> data = new ArrayList<>();
    private String username, status, nama, title, sebagai, id_pegawai, un_ybs;
    String url, tahun;
    private Spinner spTahun;

    private LineChartView chart;
    private LineChartData dataChart;
    private int numberOfLines = 1;
    private int maxNumberOfLines = 4;
    private int numberOfPoints = 13;

    float[][] randomNumbersTab = new float[maxNumberOfLines][numberOfPoints];

    private boolean hasAxes = true;
    private boolean hasAxesNames = true;
    private boolean hasLines = true;
    private boolean hasPoints = true;
    private ValueShape shape = ValueShape.CIRCLE;
    private boolean isFilled = false;
    private boolean hasLabels = false;
    private boolean isCubic = false;
    private boolean hasLabelForSelected = false;
    private boolean pointsHaveDifferentColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_satpam_penilaian);
        session = new SessionManager(getApplicationContext());
        loading = new CatLoadingView();
        user = session.getData();
        username = user.get(SessionManager.KEY_USERNAME);
        status = user.get(SessionManager.KEY_STATUS);
        nama = user.get(SessionManager.KEY_NAMA);
        title = getIntent().getStringExtra("title");
        setupActionBar();
        sebagai = status;
        id_pegawai =  user.get(SessionManager.KEY_ID_PEGAWAI);
        un_ybs =  username;
        lLayout = new GridLayoutManager(SatpamPenilaianActivity.this, 4);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        spTahun = (Spinner) findViewById(R.id.spThn);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(lLayout);
        Calendar c = Calendar.getInstance();
        tahun = new SimpleDateFormat("yyyy").format(c.getTime());
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(SatpamPenilaianActivity.this, R.array.thn, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTahun.setAdapter(adapter);
        if (!tahun.equals(null)) {
            int spinnerPosition = adapter.getPosition(tahun);
            spTahun.setSelection(spinnerPosition);
        }
        spTahun.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View v, int i, long l) {
                System.out.println("zzz..."+adapterView.getSelectedItem());
                url = getResources().getString(R.string.ipaddr)+"act=getAllPenilaianByUsernameAndTahun&username="+username+"&thn="+adapterView.getSelectedItem();
                System.out.println("xxx.."+url);
                setRecyclerView();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setupChart(int mulai, int jumlah){
        chart = (LineChartView) findViewById(R.id.chart);
        chart.setOnValueTouchListener(new ValueTouchListener());

        // Generate some random values.
//        generateValues();

        generateData(mulai, jumlah);

        // Disable viewport recalculations, see toggleCubic() method for more info.
        chart.setViewportCalculationEnabled(false);

        resetViewport();
    }

    private void generateValues(int bulan, double nilai) {
        for (int i = 0; i < maxNumberOfLines; ++i) {
//            for (int j = 0; j < numberOfPoints; ++j) {
//                randomNumbersTab[i][bulan] = (float) nilai;
//            }
            randomNumbersTab[i][bulan] = (float) nilai;
        }
    }

    private void resetViewport() {
        // Reset viewport height range to (0,100)
        final Viewport v = new Viewport(chart.getMaximumViewport());
        v.bottom = 0;
        v.top = 100;
        v.left = 0;
        v.right = 12;
        chart.setMaximumViewport(v);
        chart.setCurrentViewport(v);
    }

    private void generateData(int mulai, int jumlah) {

        List<Line> lines = new ArrayList<Line>();
        for (int i = 0; i < numberOfLines; ++i) {

            List<PointValue> values = new ArrayList<PointValue>();
            for (int j = mulai; j < jumlah; ++j) {
                values.add(new PointValue(j, randomNumbersTab[i][j]));
            }

            Line line = new Line(values);
            line.setColor(ChartUtils.COLORS[i]);
            line.setShape(shape);
            line.setCubic(isCubic);
            line.setFilled(isFilled);
            line.setHasLabels(hasLabels);
            line.setHasLabelsOnlyForSelected(hasLabelForSelected);
            line.setHasLines(hasLines);
            line.setHasPoints(hasPoints);
            if (pointsHaveDifferentColor){
                line.setPointColor(ChartUtils.COLORS[(i + 1) % ChartUtils.COLORS.length]);
            }
            lines.add(line);
        }

        dataChart = new LineChartData(lines);

        if (hasAxes) {
            Axis axisX = new Axis();
            Axis axisY = new Axis().setHasLines(true);
            if (hasAxesNames) {
                axisX.setName("BULAN");
                axisY.setName("NILAI");
            }
            dataChart.setAxisXBottom(axisX);
            dataChart.setAxisYLeft(axisY);
        } else {
            dataChart.setAxisXBottom(null);
            dataChart.setAxisYLeft(null);
        }

        dataChart.setBaseValue(Float.NEGATIVE_INFINITY);
        chart.setLineChartData(dataChart);

    }

    public void setRecyclerView(){
        data = getAllItemList(url);
        adapter = new CarakaPenilaianAdapter(SatpamPenilaianActivity.this, data, new CarakaPenilaianAdapter.OnItemClickListener() {
            @Override
            public void onClick(Penilaian item) {
                String msg;
                if(sebagai.equalsIgnoreCase("caraka")){
                    msg = "Keramahan: "+item.getKeramahan()+"\n"+
                            "Kesigapan: "+item.getKesigapan()+"\n"+
                            "Kerajinan: "+item.getKerajinan()+"\n"+
                            "Kebersihan: "+item.getKebersihan();
                }else{
                    msg = "Keramahan: "+item.getKeramahan()+"\n"+
                            "Kesigapan: "+item.getKesigapan()+"\n"+
                            "Layanan Tamu: "+item.getLayanan_tamu()+"\n"+
                            "Patroli: "+item.getPatroli();
                }
                detailNilai(msg);
            }
        }, sebagai);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }

    void detailNilai(String msg){
        new AlertDialog.Builder(SatpamPenilaianActivity.this)
                .setTitle("DETAIL NILAI")
                .setMessage(msg)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //
                    }
                })
                .show();
    }

    private List<Penilaian> getAllItemList(final String url){
        final List<Penilaian> allItems = new ArrayList<>();
        loading.show(getSupportFragmentManager(),"");

        RequestQueue queue = Volley.newRequestQueue(SatpamPenilaianActivity.this);
        StringRequest strReq = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String >(){
                    @Override
                    public void onResponse(String response) {
                        loading.dismiss();
                        Log.d("LOG","Response: "+response.toString());
                        try {
                            JSONArray array_sucess = new JSONArray(response);
                            int size = array_sucess.length();
                            allItems.clear();
                            Penilaian model;
                            List<Integer> nilai = new ArrayList<>();
                            for(int i = 0; i<size; i++){
                                JSONObject data = array_sucess.getJSONObject(i);
                                int id_penilaian = Integer.parseInt(data.getString("id_penilaian"));
                                int id_presensi = Integer.parseInt(data.getString("id_presensi"));
                                String periode = data.getString("periode");
                                Double absensi = Double.parseDouble(data.getString("absensi"));
                                String id_pegawai = data.getString("id_pegawai");
                                int kesigapan= Integer.parseInt(data.getString("kesigapan"));
                                int patroli = Integer.parseInt(data.getString("patroli"));
                                int layanan_tamu = Integer.parseInt(data.getString("layanan_tamu"));
                                int keramahan = Integer.parseInt(data.getString("keramahan"));
                                int kerajinan= Integer.parseInt(data.getString("kerajinan"));
                                int kebersihan= Integer.parseInt(data.getString("kebersihan"));
                                Double nilai_akhir= Double.parseDouble(data.getString("nilai_akhir"));
                                model = new Penilaian(id_penilaian,id_presensi,periode,absensi,id_pegawai,kesigapan,patroli,layanan_tamu,keramahan,kerajinan,kebersihan,nilai_akhir);
                                allItems.add(model);
                                adapter.notifyDataSetChanged();

                                int bulan = Integer.parseInt(periode.substring(6));
                                generateValues(bulan, nilai_akhir);
                                nilai.add(bulan);
                            }

                            if(nilai.size()>0){
                                setupChart(nilai.get(0), nilai.size()+nilai.get(0));
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                Log.e("ERROR", "Error: " + error.getMessage());
                Toast.makeText(SatpamPenilaianActivity.this,
                        "Gagal Menampilkan Data.", Toast.LENGTH_LONG).show();
            }
        });

        // Adding request to request queue
        queue.add(strReq);

        return allItems;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_no_animation, R.anim.push_out_right);
    }

    void setupActionBar(){
        getSupportActionBar().setElevation(3);
        getSupportActionBar().setTitle(title.toUpperCase());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

    }

    private class ValueTouchListener implements LineChartOnValueSelectListener {

        @Override
        public void onValueSelected(int lineIndex, int pointIndex, PointValue value) {
            Toast.makeText(SatpamPenilaianActivity.this, value.toString(), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onValueDeselected() {
            // TODO Auto-generated method stub

        }

    }
}
