package com.bungkhus.ministudio.asih.model;

import android.graphics.drawable.Drawable;

/**
 * Created by bungkhus on 8/6/16.
 */
public class Menu {

    int id;
    String nama;
    Drawable gambar;

    public Menu() {
    }

    public Menu(int id, String nama, Drawable gambar) {
        this.id = id;
        this.nama = nama;
        this.gambar = gambar;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Drawable getGambar() {
        return gambar;
    }

    public void setGambar(Drawable gambar) {
        this.gambar = gambar;
    }
}
