package com.bungkhus.ministudio.asih.view.tatausaha.fragment;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import lecho.lib.hellocharts.listener.PieChartOnValueSelectListener;
import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.Chart;
import lecho.lib.hellocharts.view.PieChartView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.model.RekapPresensi;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class KatuCarakaPresensiFragment extends Fragment {

    private PieChartView chart;
    private PieChartData data;
    private CatLoadingView loading;

    private boolean hasLabels = true;
    private boolean hasLabelsOutside = false;
    private boolean hasCenterCircle = false;
    private boolean isExploded = false;
    private boolean hasLabelForSelected = false;
    private List<RekapPresensi> rekaps;
    private Spinner spBln, spThn;
    private String bulan, tahun;

    public KatuCarakaPresensiFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_katu_caraka_presensi, container, false);
        setHasOptionsMenu(false);

        loading = new CatLoadingView();
        spBln = (Spinner) rootView.findViewById(R.id.spBln);
        spThn = (Spinner) rootView.findViewById(R.id.spThn);
        chart = (PieChartView) rootView.findViewById(R.id.chart);
        chart.setOnValueTouchListener(new ValueTouchListener());

        final Calendar c = Calendar.getInstance();
        bulan = new SimpleDateFormat("MM").format(c.getTime());
        tahun = new SimpleDateFormat("yyyy").format(c.getTime());

        spBln.setSelection((Integer.parseInt(bulan) - 1));

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.thn, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spThn.setAdapter(adapter);
        if (!tahun.equals(null)) {
            int spinnerPosition = adapter.getPosition(tahun);
            spThn.setSelection(spinnerPosition);
        }

        spBln.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int bln = adapterView.getSelectedItemPosition()+1;
                rekaps = getAllItemList(getResources().getString(R.string.ipaddr)+"act=getRekapPresensi&bln="+bln+"&thn="+spThn.getSelectedItem()+"&bagian=caraka");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spThn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int bln = spBln.getSelectedItemPosition()+1;
                rekaps = getAllItemList(getResources().getString(R.string.ipaddr)+"act=getRekapPresensi&bln="+bln+"&thn="+adapterView.getSelectedItem()+"&bagian=caraka");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        return rootView;
    }

    private List<RekapPresensi> getAllItemList(final String url){

        final List<RekapPresensi> allItems = new ArrayList<>();
//        loading.show(getFragmentManager(), "");

        RequestQueue queue = Volley.newRequestQueue(getContext());
        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                loading.dismiss();
                Log.d("LOG", "Response: " + response.toString());

                try {
                    JSONArray array_success = new JSONArray(response);
                    int size = array_success.length();
                    allItems.clear();
                    RekapPresensi model;
                    for (int i = 0; i<size; i++){
                        JSONObject data = array_success.getJSONObject(i);
                        int hadir = Integer.parseInt(data.getString("hadir"));
                        int ijin = Integer.parseInt(data.getString("ijin"));
                        int alpha = Integer.parseInt(data.getString("alpha"));
                        int total = Integer.parseInt(data.getString("total"));
                        model = new RekapPresensi(hadir, ijin, alpha, total);
                        allItems.add(model);
                    }
                    generateData(allItems);
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                loading.dismiss();
                Log.e("ERROR", "Error: " + error.getMessage());
                Toast.makeText(getContext(),
                        "Gagal Menampilkan Data.", Toast.LENGTH_LONG).show();
            }
        });

        // Adding request to request queue
        queue.add(strReq);

        return allItems;
    }

    // MENU
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.pie_chart, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_reset) {
            reset();
            generateData(rekaps);
            toggleLabels();
            return true;
        }
        if (id == R.id.action_explode) {
            explodeChart();
            return true;
        }
        if (id == R.id.action_center_circle) {
            hasCenterCircle = !hasCenterCircle;
            generateData(rekaps);
            return true;
        }
        if (id == R.id.action_toggle_labels) {
            toggleLabels();
            return true;
        }
        if (id == R.id.action_toggle_labels_outside) {
            toggleLabelsOutside();
            return true;
        }
        if (id == R.id.action_animate) {
            prepareDataAnimation();
            chart.startDataAnimation();
            return true;
        }
        if (id == R.id.action_toggle_selection_mode) {
            toggleLabelForSelected();
            Toast.makeText(getActivity(),
                    "Selection mode set to " + chart.isValueSelectionEnabled() + " select any point.",
                    Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void reset() {
        chart.setCircleFillRatio(1.0f);
        hasLabels = false;
        hasLabelsOutside = true;
        hasCenterCircle = false;
        isExploded = false;
        hasLabelForSelected = false;
    }

    private void generateData(List<RekapPresensi> rekapPresensis) {
        List<SliceValue> values = new ArrayList<SliceValue>();

        SliceValue hadir = new SliceValue((float) rekapPresensis.get(0).getHadir(), ChartUtils.COLOR_GREEN);
        hadir.setLabel(rekapPresensis.get(0).getHadir()+" HADIR ("+(rekapPresensis.get(0).getHadir()*100)/rekapPresensis.get(0).getTotal()+"%)");
        values.add(hadir);

        SliceValue ijin = new SliceValue((float) rekapPresensis.get(0).getIjin(), ChartUtils.COLOR_ORANGE);
        ijin.setLabel(rekapPresensis.get(0).getIjin()+" IJIN ("+(rekapPresensis.get(0).getIjin()*100)/rekapPresensis.get(0).getTotal()+"%)");
        values.add(ijin);

        SliceValue alpha = new SliceValue((float) rekapPresensis.get(0).getAlpha(), ChartUtils.COLOR_RED);
        alpha.setLabel(rekapPresensis.get(0).getAlpha()+" ALPHA ("+(rekapPresensis.get(0).getAlpha()*100)/rekapPresensis.get(0).getTotal()+"%)");
        values.add(alpha);


//        for (int i = 0; i < numValues; ++i) {
//            SliceValue sliceValue = new SliceValue((float) rekapPresensis.get(0).getAlpha(), ChartUtils.pickColor());
//            sliceValue.setLabel("ini"+i);
//            values.add(sliceValue);
//        }

        data = new PieChartData(values);
        data.setHasLabels(hasLabels);
        data.setHasLabelsOnlyForSelected(hasLabelForSelected);
        data.setHasLabelsOutside(hasLabelsOutside);
        data.setHasCenterCircle(hasCenterCircle);

        if (isExploded) {
            data.setSlicesSpacing(24);
        }

//        chart.setCircleFillRatio(0.7f);
//        hasLabels = !hasLabels;
//
//        if (hasLabels) {
//            hasLabelForSelected = false;
//            chart.setValueSelectionEnabled(hasLabelForSelected);
//
//            if (hasLabelsOutside) {
//                chart.setCircleFillRatio(0.7f);
//            } else {
//                chart.setCircleFillRatio(1.0f);
//            }
//        }

        chart.setPieChartData(data);
    }

    private void explodeChart() {
        isExploded = !isExploded;
        generateData(rekaps);

    }

    private void toggleLabelsOutside() {
        // has labels have to be true:P
        hasLabelsOutside = !hasLabelsOutside;
        if (hasLabelsOutside) {
            hasLabels = true;
            hasLabelForSelected = false;
            chart.setValueSelectionEnabled(hasLabelForSelected);
        }

        if (hasLabelsOutside) {
            chart.setCircleFillRatio(0.7f);
        } else {
            chart.setCircleFillRatio(1.0f);
        }

        generateData(rekaps);

    }

    private void toggleLabels() {
        hasLabels = !hasLabels;

        if (hasLabels) {
            hasLabelForSelected = false;
            chart.setValueSelectionEnabled(hasLabelForSelected);

            if (hasLabelsOutside) {
                chart.setCircleFillRatio(0.7f);
            } else {
                chart.setCircleFillRatio(1.0f);
            }
        }

        generateData(rekaps);
    }

    private void toggleLabelForSelected() {
        hasLabelForSelected = !hasLabelForSelected;

        chart.setValueSelectionEnabled(hasLabelForSelected);

        if (hasLabelForSelected) {
            hasLabels = false;
            hasLabelsOutside = false;

            if (hasLabelsOutside) {
                chart.setCircleFillRatio(0.7f);
            } else {
                chart.setCircleFillRatio(1.0f);
            }
        }

        generateData(rekaps);
    }

    /**
     * To animate values you have to change targets values and then call {@link Chart#startDataAnimation()}
     * method(don't confuse with View.animate()).
     */
    private void prepareDataAnimation() {
        for (SliceValue value : data.getValues()) {
            value.setTarget((float) Math.random() * 30 + 15);
        }
    }

    private class ValueTouchListener implements PieChartOnValueSelectListener {

        @Override
        public void onValueSelected(int arcIndex, SliceValue value) {
            Toast.makeText(getActivity(), "Selected: " + value.getLabelAsChars(), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onValueDeselected() {
            // TODO Auto-generated method stub

        }

    }

}
