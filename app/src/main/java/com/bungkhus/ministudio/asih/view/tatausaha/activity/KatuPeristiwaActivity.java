package com.bungkhus.ministudio.asih.view.tatausaha.activity;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.adapter.tatausaha.KatuPeristiwaAdapter;
import com.bungkhus.ministudio.asih.helper.DateHelper;
import com.bungkhus.ministudio.asih.helper.SessionManager;
import com.bungkhus.ministudio.asih.model.Peristiwa;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class KatuPeristiwaActivity extends AppCompatActivity {

    private HashMap<String, String> user;
    private String username, status, nama, title;
    private SessionManager session;
    private CatLoadingView loading;
    private RecyclerView recyclerView;
    private List<Peristiwa> data = new ArrayList<>();
    private KatuPeristiwaAdapter adapter;
    String url;
    private Spinner spBln, spThn;
    private String bulan, tahun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_katu_peristiwa);
        session = new SessionManager(getApplicationContext());
        loading = new CatLoadingView();
        user = session.getData();
        spBln = (Spinner) findViewById(R.id.spBln);
        spThn = (Spinner) findViewById(R.id.spThn);
        username = user.get(SessionManager.KEY_USERNAME);
        status = user.get(SessionManager.KEY_STATUS);
        nama = user.get(SessionManager.KEY_NAMA);
        title = getIntent().getStringExtra("title");
        setupActionBar();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(KatuPeristiwaActivity.this));
        setupSpinner();
    }

    private void setupSpinner(){
        final Calendar c = Calendar.getInstance();
        bulan = new SimpleDateFormat("MM").format(c.getTime());
        tahun = new SimpleDateFormat("yyyy").format(c.getTime());

        spBln.setSelection((Integer.parseInt(bulan) - 1));
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(KatuPeristiwaActivity.this, R.array.thn, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spThn.setAdapter(adapter);
        if (!tahun.equals(null)) {
            int spinnerPosition = adapter.getPosition(tahun);
            spThn.setSelection(spinnerPosition);
        }

        spBln.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int bln = adapterView.getSelectedItemPosition()+1;
                url = getResources().getString(R.string.ipaddr)+"act=getAllPeristiwaByPeriode&bln="+bln+"&thn="+spThn.getSelectedItem();
                setRecyclerView(url);
                System.out.println("anu.."+url);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spThn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int bln = spBln.getSelectedItemPosition()+1;
                url = getResources().getString(R.string.ipaddr)+"act=getAllPeristiwaByPeriode&bln="+bln+"&thn="+adapterView.getSelectedItem();
                setRecyclerView(url);
                System.out.println("emm.."+url);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void setRecyclerView(String url) {
        data = getAllItemList(url);
        adapter = new KatuPeristiwaAdapter(KatuPeristiwaActivity.this, data, new KatuPeristiwaAdapter.OnItemClickListener() {
            @Override
            public void onClick(Peristiwa item) {

            }
        }, "katu");
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.rekap, menu);
        menu.removeItem(R.id.action_date);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_rekap:
                int bulan = spBln.getSelectedItemPosition()+1;
                getRekap(getResources().getString(R.string.ipaddr)+"act=getRekapPeristiwaPerBulan&thn="+spThn.getSelectedItem()+"&bln="+bulan);
                break;
        }
        return true;
    }

    private void getRekap(final String url){
        loading.show(getSupportFragmentManager(),"");

        RequestQueue queue = Volley.newRequestQueue(KatuPeristiwaActivity.this);
        StringRequest strReq = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String >(){
                    @Override
                    public void onResponse(String response) {
                        loading.dismiss();
                        Log.d("LOG","Response: "+response.toString());
                        try {
                            JSONArray array_sucess = new JSONArray(response);
                            int size = array_sucess.length();
                            String msg = "";
                            for(int i = 0; i<size; i++){
                                JSONObject data = array_sucess.getJSONObject(i);
                                Date bulan = DateHelper.stringToDate(data.getString("bulan"), new SimpleDateFormat("M"));
                                String tahun = data.getString("tahun");
                                String total = data.getString("total");
                                msg = msg+DateHelper.dateToString(bulan, new SimpleDateFormat("MMMM"))+" "
                                        +tahun
                                        +"\nTotal Peristiwa: "+total;
                            }
                            if(size == 0){
                                msg = "Tidak ada peristiwa.";
                            }
                            showRekapitulasi(msg);
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                Log.e("ERROR", "Error: " + error.getMessage());
                Toast.makeText(KatuPeristiwaActivity.this,
                        "Gagal Menampilkan Data.", Toast.LENGTH_LONG).show();
            }
        });

        // Adding request to request queue
        queue.add(strReq);
    }

    void showRekapitulasi(String msg){
        new AlertDialog.Builder(KatuPeristiwaActivity.this)
                .setTitle("Rekapitulasi Peristiwa")
                .setMessage(msg)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //
                    }
                })
                .show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_no_animation, R.anim.push_out_right);
    }

    void setupActionBar(){
        getSupportActionBar().setElevation(3);
        getSupportActionBar().setTitle(title.toUpperCase());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

    }

    private List<Peristiwa> getAllItemList(final String url){

        final List<Peristiwa> allItems = new ArrayList<>();
//        loading.show(getSupportFragmentManager(), "");

        RequestQueue queue = Volley.newRequestQueue(KatuPeristiwaActivity.this);
        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                loading.dismiss();
                Log.d("LOG", "Response: " + response.toString());

                try {
                    JSONArray array_success = new JSONArray(response);
                    int size = array_success.length();
                    allItems.clear();
                    Peristiwa model;
                    for (int i = 0; i<size; i++){
                        JSONObject data = array_success.getJSONObject(i);
                        int id_peristiwa = Integer.parseInt(data.getString("id_peristiwa"));
                        Date tgl_peristiwa = DateHelper.stringToDate(data.getString("tgl_peristiwa"), new SimpleDateFormat("yyyy-MM-dd"));
                        String jam_peristiwa = data.getString("jam_peristiwa");
                        String kejadian = data.getString("kejadian");
                        String deskripsi = data.getString("deskripsi");
                        String video = data.getString("video");
                        String status_penyelesaian = data.getString("status_penyelesaian");
                        String username = data.getString("username");
                        String total = data.getString("total");
                        JSONArray array_saksi = data.getJSONArray("saksi");
                        ArrayList<String> saksi = new ArrayList<>();
                        for (int j = 0; j < array_saksi.length(); j++){
                            saksi.add(array_saksi.get(j).toString());
                        }
                        model = new Peristiwa();
                        model.setId_peristiwa(id_peristiwa);
                        model.setTgl_peristiwa(tgl_peristiwa);
                        model.setJam_peristiwa(jam_peristiwa);
                        model.setKejadian(kejadian);
                        model.setDeskripsi(deskripsi);
                        model.setVideo(video);
                        model.setStatus_penyelesaian(status_penyelesaian);
                        model.setUsername(username);
                        model.setTotal(total);
                        model.setSaksi(saksi);
                        String gambar = data.getString("gambar");
                        model.setFoto(gambar);
                        allItems.add(model);
                        adapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                loading.dismiss();
                Log.e("ERROR", "Error: " + error.getMessage());
                Toast.makeText(KatuPeristiwaActivity.this,
                        "Gagal Menampilkan Data.", Toast.LENGTH_LONG).show();
            }
        });

        // Adding request to request queue
        queue.add(strReq);

        return allItems;
    }
}
