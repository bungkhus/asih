package com.bungkhus.ministudio.asih.view.tatausaha.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.adapter.caraka.CarakaJadwalAdapter;
import com.bungkhus.ministudio.asih.helper.DateHelper;
import com.bungkhus.ministudio.asih.helper.SessionManager;
import com.bungkhus.ministudio.asih.model.Jadwal;
import com.bungkhus.ministudio.asih.view.tatausaha.activity.KatuAddJadwalPegawaiActivity;
import com.bungkhus.ministudio.asih.view.tatausaha.activity.KatuPresensiPegawaiActivity;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class KatuJadwalPegawaiFragment extends Fragment {

    private HashMap<String, String> user;
    private String username, status, nama, id_pegawai;
    private SessionManager session;
    private CatLoadingView loading;
    private RecyclerView recyclerView;
    private List<Jadwal> data = new ArrayList<>();
    private CarakaJadwalAdapter adapter;
    String url;


    public KatuJadwalPegawaiFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_katu_jadwal_pegawai, container, false);
        setHasOptionsMenu(true);
        session = new SessionManager(getContext());
        loading = new CatLoadingView();
        user = session.getData();
        username = user.get(SessionManager.KEY_USERNAME);
        status = user.get(SessionManager.KEY_STATUS);
        nama = user.get(SessionManager.KEY_NAMA);
        id_pegawai = getActivity().getIntent().getStringExtra("id_pegawai");
        url = getResources().getString(R.string.ipaddr)+"act=getAllJadwalByUsername&username="+getActivity().getIntent().getStringExtra("un_ybs");
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        setRecyclerView();

        return view;
    }

    public void setRecyclerView() {
        data = getAllItemList(url);
        adapter = new CarakaJadwalAdapter(getContext(), data, new CarakaJadwalAdapter.OnItemClickListener() {
            @Override
            public void onClick(Jadwal item) {
                goToPresensi(KatuPresensiPegawaiActivity.class, item.getNama().toUpperCase(), item.getUsername(), item.getBol(), String.valueOf(item.getId_jadwal()), DateHelper.dateToString(item.getTgl_mulai(), new SimpleDateFormat("dd MMM yyyy")), DateHelper.dateToString(item.getTgl_selesai(), new SimpleDateFormat("dd MMM yyyy")), item.getTugas());
            }
        }, getActivity().getIntent().getStringExtra("sebagai"));
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }

    void goToPresensi(Class<?> cls, String title, String un_ybs, String bol, String id, String tgl_mulai, String tgl_selesai, String tugas){
        Intent i = new Intent(getContext(), cls);
        i.putExtra("title", title);
        i.putExtra("sebagai", getActivity().getIntent().getStringExtra("sebagai"));
        i.putExtra("un_ybs", un_ybs);
        i.putExtra("bol", bol);
        i.putExtra("id_jadwal", id);
        i.putExtra("tgl_mulai", tgl_mulai);
        i.putExtra("tgl_selesai", tgl_selesai);
        i.putExtra("tugas", tugas);
        i.putExtra("id_pegawai", id_pegawai);
        startActivity(i);
        getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.activity_no_animation);
    }

    private List<Jadwal> getAllItemList(final String url){

        final List<Jadwal> allItems = new ArrayList<>();
        loading.show(getFragmentManager(), "");

        RequestQueue queue = Volley.newRequestQueue(getContext());
        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                loading.dismiss();
                Log.d("LOG", "Response: " + response.toString());

                try {
                    JSONArray array_success = new JSONArray(response);
                    int size = array_success.length();
                    allItems.clear();
                    Jadwal model;
                    for (int i = 0; i<size; i++){
                        JSONObject data = array_success.getJSONObject(i);
                        int id_jadwal = Integer.parseInt(data.getString("id_jadwal"));
                        Date tgl_mulai = DateHelper.stringToDate(data.getString("tgl_mulai"), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
                        Date tgl_selesai = DateHelper.stringToDate(data.getString("tgl_selesai"), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
                        String tugas = data.getString("tugas");
                        String nama= data.getString("nama");
                        String username = data.getString("username");
                        String bol = data.getString("bol");
                        model = new Jadwal(id_jadwal,tgl_mulai,tgl_selesai,tugas,username,nama, bol);
                        allItems.add(model);
                        adapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                Log.e("ERROR", "Error: " + error.getMessage());
                Toast.makeText(getContext(),
                        "Gagal Menampilkan Data.", Toast.LENGTH_LONG).show();
            }
        });

        // Adding request to request queue
        queue.add(strReq);

        return allItems;
    }

    void goTo(Class<?> cls, String title){
        Intent i = new Intent(getContext(), cls);
        i.putExtra("title", title);
        i.putExtra("sebagai", getActivity().getIntent().getStringExtra("sebagai"));
        i.putExtra("un_ybs", getActivity().getIntent().getStringExtra("un_ybs"));
        startActivity(i);
        getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.activity_no_animation);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.add, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub

        switch (item.getItemId()) {

            case R.id.action_add:
                goTo(KatuAddJadwalPegawaiActivity.class, "TAMBAH JADWAL "+getActivity().getIntent().getStringExtra("title"));
                break;
        }
        return true;
    }

}
