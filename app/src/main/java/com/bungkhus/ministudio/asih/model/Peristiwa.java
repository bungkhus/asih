package com.bungkhus.ministudio.asih.model;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by bungkhus on 8/7/16.
 */
public class Peristiwa {

    int id_peristiwa;
    Date tgl_peristiwa;
    String jam_peristiwa;
    String kejadian;
    String deskripsi;
    String video;
    String foto;
    String status_penyelesaian;
    String username;
    String total;
    ArrayList<String> saksi;

    public Peristiwa() {
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public int getId_peristiwa() {
        return id_peristiwa;
    }

    public void setId_peristiwa(int id_peristiwa) {
        this.id_peristiwa = id_peristiwa;
    }

    public Date getTgl_peristiwa() {
        return tgl_peristiwa;
    }

    public void setTgl_peristiwa(Date tgl_peristiwa) {
        this.tgl_peristiwa = tgl_peristiwa;
    }

    public String getJam_peristiwa() {
        return jam_peristiwa;
    }

    public void setJam_peristiwa(String jam_peristiwa) {
        this.jam_peristiwa = jam_peristiwa;
    }

    public String getKejadian() {
        return kejadian;
    }

    public void setKejadian(String kejadian) {
        this.kejadian = kejadian;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getStatus_penyelesaian() {
        return status_penyelesaian;
    }

    public void setStatus_penyelesaian(String status_penyelesaian) {
        this.status_penyelesaian = status_penyelesaian;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public ArrayList<String> getSaksi() {
        return saksi;
    }

    public void setSaksi(ArrayList<String> saksi) {
        this.saksi = saksi;
    }
}
