package com.bungkhus.ministudio.asih.adapter.tatausaha;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bungkhus.ministudio.asih.R;
import com.bungkhus.ministudio.asih.model.Pegawai;

import java.util.List;

/**
 * Created by bungkhus on 8/7/16.
 */
public class KatuPegawaiAdapter extends RecyclerView.Adapter<KatuPegawaiAdapter.RecyclerViewHolders> {

    private final OnItemClickListener listener;
    private List<Pegawai> itemList;
    private Context context;

    public KatuPegawaiAdapter(Context context, List<Pegawai> itemList, OnItemClickListener listener) {
        this.itemList = itemList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_pegawai, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        holder.click(itemList.get(position), listener);
        holder.countryName.setText(itemList.get(position).getNama());
        String url = context.getResources().getString(R.string.ipaddr_media)+itemList.get(position).getFoto();

        Glide
                .with(context)
                .load(url)
                .centerCrop()
                .placeholder(R.drawable.noimage)
                .crossFade()
                .into(holder.countryPhoto);
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView countryName;
        public ImageView countryPhoto;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            countryName = (TextView)itemView.findViewById(R.id.country_name);
            countryPhoto = (ImageView)itemView.findViewById(R.id.country_photo);
        }

        public void click(final Pegawai item, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(item);
                }
            });
        }

        @Override
        public void onClick(View view) {

        }
    }

    public interface OnItemClickListener {
        void onClick(Pegawai item);
    }
}