package com.bungkhus.ministudio.asih.model;

/**
 * Created by bungkhus on 8/12/16.
 */
public class RekapPresensi {

    int hadir, ijin, alpha, total;

    public RekapPresensi() {
    }

    public RekapPresensi(int hadir, int ijin, int alpha, int total) {
        this.hadir = hadir;
        this.ijin = ijin;
        this.alpha = alpha;
        this.total = total;
    }

    public int getHadir() {
        return hadir;
    }

    public void setHadir(int hadir) {
        this.hadir = hadir;
    }

    public int getIjin() {
        return ijin;
    }

    public void setIjin(int ijin) {
        this.ijin = ijin;
    }

    public int getAlpha() {
        return alpha;
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
